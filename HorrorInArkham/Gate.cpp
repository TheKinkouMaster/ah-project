#include "Gate.h"



CGate::CGate()
{
	Reset();
}


CGate::~CGate()
{
}

SDimensionType CGate::GiveDimensionStructure()
{
	return m_sDimension;
}

string CGate::GiveDimension()
{
	if (m_sDimension.circle == true)   return "Circle";
	if (m_sDimension.cross == true)    return "Cross";
	if (m_sDimension.diamond == true)  return "Diamond";
	if (m_sDimension.hexagon == true)  return "Hexagon";
	if (m_sDimension.line == true)     return "Line";
	if (m_sDimension.moon == true)     return "Moon";
	if (m_sDimension.square == true)   return "Square";
	if (m_sDimension.star == true)     return "Star";
	if (m_sDimension.triangle == true) return "Triangle";
	return "error";
}

string CGate::GiveDestination()
{
	return m_sDestination;
}

bool CGate::ChangeDestination(string newDestination)
{
	m_sDestination = newDestination;
	return true;
}

bool CGate::ChangeDimenstion(string newDimension)
{
	ResetDimension();
	if (newDimension == "Circle")   m_sDimension.circle   = true;
	if (newDimension == "Cross")    m_sDimension.cross    = true;
	if (newDimension == "Diamond")  m_sDimension.diamond  = true;
	if (newDimension == "Hexagon")  m_sDimension.hexagon  = true;
	if (newDimension == "Line")     m_sDimension.line     = true;
	if (newDimension == "Moon")     m_sDimension.moon     = true;
	if (newDimension == "Square")   m_sDimension.square   = true;
	if (newDimension == "Star")     m_sDimension.star     = true;
	if (newDimension == "Triangle") m_sDimension.triangle = true;
	return true;
}

bool CGate::Reset()
{
	ResetDestination();
	ResetDimension();
	m_iHP = 0;
	return true; 
}

bool CGate::ResetDestination()
{
	m_sDestination = "NULL";
	return true;
}

bool CGate::ResetDimension()
{
	m_sDimension.circle   = false;
	m_sDimension.cross    = false;
	m_sDimension.diamond  = false;
	m_sDimension.hexagon  = false;
	m_sDimension.line     = false;
	m_sDimension.moon     = false;
	m_sDimension.square   = false;
	m_sDimension.star     = false;
	m_sDimension.triangle = false;
	return true;
}
