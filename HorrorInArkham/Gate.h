#pragma once
#include "GameObject.h"
class CGate :
	public CGameObject
{
private:
	SDimensionType m_sDimension;
	string         m_sDestination;
	int            m_iHP;
public:
	//Accessors
	SDimensionType GiveDimensionStructure();
	string         GiveDimension();
	string         GiveDestination();
	inline int     GiveHP() { return m_iHP; };

	//Modifiers
	bool ChangeDestination(string newDestination);
	bool ChangeDimenstion(string newDimension); //Cross, Moon, etc
	bool Reset();
	bool ResetDestination();
	bool ResetDimension();

	inline bool ChangeHP(int newValue) { m_iHP = newValue; return true; };

	//Methods
	
	//C/D
	CGate();
	~CGate();
};

