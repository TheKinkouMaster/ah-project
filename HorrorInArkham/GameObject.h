#pragma once
#include <string>
#include "MyMap.h"
#include "AdditionalFunctionsAndStructures.h"

using namespace std;
class CGameObject
{
protected:
	string m_sName;
	string m_sCurrentLocation;
public:
	//Accessors
	inline string GiveName()         { return m_sName; };
	inline string GiveLocationName() { return m_sCurrentLocation; };
	//Modifiers
	inline void ChangeName    (string newName) { m_sName = newName; };
	inline void ChangeLocation(string newName) { m_sCurrentLocation = newName; };
	//C/D
	CGameObject (const char *name, string location = "BOX");
	CGameObject ();
	~CGameObject();
};

