#pragma once
#include "MapLocation.h"
#include "MapObject.h"
#include "MapOutterWorld.h"
#include "MapSideLocation.h"
#include "MapStreet.h"
#include "Constans.cpp"
#include <string>

class CMyMap
{
protected:
	CMapLocation     *m_CMAPLocations[maxMapLocations];
	CMapOutterWorld  *m_CMAPOutterLocations[maxMapOutterLocations];
	CMapSideLocation *m_CMAPSideLocations[maxMapSideLocations];
	CMapStreet       *m_CMAPStreets[maxMapStreets];

	void *panicTrait[11];
protected:
	int returnPosition(string name);
public:
	//Accessors
	string GiveLocation            (int number);

	string GiveOutterLocation      (int number);

	string GiveSideLocation        (int number);
	string GiveSideLocation        (string streetname, int number);
	bool   GiveSideLocation        (string streetname, string arraystr[]);

	string GiveMainLocation        (string streetname, int number);
	string GiveStreet              (int number);
	string GiveStreetAttached      (string name);

	string GivePath                (string direction, string locationName);

	bool GiveSideLocations         (bool Safety, string arraystr[]);

	string GiveSideLocationResource(int which, string locationName);

	//////////////////////////checkers
	bool CheckIfTheresStreet       (string name);
	bool CheckIfTheresSideLocation (string name);
	bool CheckIfTheresLocation     (string name);
	bool CheckIfTheresOutterWorld  (string name);

	//Modifiers
	bool AddNewStreet              (string streetName);
	bool AddNewSideLocation        (string SideLocationName);
	bool AddNewOutterLocation      (string OutterLocationName);
	bool AddnewLocation            (string LocationName);
	
	//Methods
	bool AddNewMainLink            (string targetStreet, string linkName);
	bool AddNewSideLink            (string targetStreet, string linkName);
	bool ChangeBlackPath           (string targetStreet, string linkName);
	bool ChangeWhitePath           (string targetStreet, string linkName);

	bool ConnectSideLocation         (string targetLocation, string streetName);
	bool ChangeSideLocationsResources(string targetLocation, int res1, int res2, bool cer1, bool cer2);
	bool ChangeSideLocationSafety    (string targetLocation, bool safety);
	bool ModifyColorOfOutterWorld    (string targetWorld, const char color, bool state);

	//I/O
	bool OpenMapFromFile             (string filename);
	bool SaveMapToFIle               (string filename);

	//Constr
	CMyMap ();
	~CMyMap();
};

