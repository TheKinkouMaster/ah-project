#pragma once
#include <Windows.h>
#include <iostream>
#include <fstream>
#include "MyMap.h"

using namespace std;


class CMapEditorController
	: public CMyMap
{
private:
	fstream file;
private:
	bool WOCStreets();
	bool WOCSideLocations();
	bool WOCLocations();
	bool WOCOutterWorld();

	bool WOCStreet      (const char *streetName);
	bool WOCSideLocation(const char *sideLocation);
	bool WOCOutterWorld (const char *worldName);

	
public:
	bool MainEditorLoop();

	CMapEditorController();
	~CMapEditorController();
};

