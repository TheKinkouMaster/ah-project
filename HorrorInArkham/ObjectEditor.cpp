#include "ObjectEditor.h"



void CObjectEditor::MainEditorLoop()
{
	int choice = 0;
	string choiceS;



	while (choice != 10)
	{
		system("cls");
		cout << "Unit Editor 1.0!" << endl;
		cout << "1. Change File" << endl;
		cout << "2. Add Monster" << endl;
		cout << "3. Add Gate" << endl;
		cout << "4. Add Character" << endl;
		cout << "5. Add Card" << endl;
		cout << "6. Exit" << endl;

		cin >> choiceS;
		choice = atoi(choiceS.data());

		switch (choice)
		{
		case 1:
			cin >> choiceS;
			ChangeName(choiceS.data());
			break;
		case 2:
			AddMonster();
			break;
		case 3:
			AddGate();
			break;
		case 4:
			AddCharacter();
			break;
		case 5:
			AddMythCard();
		default:
			choice = 10;
			break;
		}
	}
}

void CObjectEditor::AddMonster()
{
	string  TMPS;
	string  TMPS2;
	int     TMPI;
	fstream file;

	file.open(filename, ios::app);

	file << "\n\nMONSTER\n";
	system("cls");

	cout << "How Many?" << endl;
	cin >> TMPI;
	file << "NUMBER " << TMPI << " EOL\n";
	cout << endl;

	cout << "What's the name of a monster? "; 
	cin >> TMPS; 
	getline(cin, TMPS2);
	TMPS.append(TMPS2);
	file << "NAME " << TMPS << " EOL\n";
	cout << endl;

	cout << "What's the movement type?" << endl;
	cin >> TMPS;
	file << "MOVEMENT " << TMPS << " EOL\n";
	cout << endl;

	cout << "What's a awereness?" << endl;
	cin >> TMPI;
	file << "AWERENESS " << TMPI << " EOL\n";
	cout << endl;

	cout << "What's the dimension type?" << endl;
	cin >> TMPS;
	file << "DIMENSION " << TMPS << " EOL\n";
	cout << endl;

	cout << "What's a HP?" << endl;
	cin >> TMPI;
	file << "HP " << TMPI << " EOL\n";
	cout << endl;

	cout << "What's a SanMOD?" << endl;
	cin >> TMPI;
	file << "SANMOD " << TMPI << " EOL\n";
	cout << endl;

	cout << "What's a SanDMG?" << endl;
	cin >> TMPI;
	file << "SANDMG " << TMPI << " EOL\n";
	cout << endl;

	cout << "What's a PhyMOD?" << endl;
	cin >> TMPI;
	file << "PHYMOD " << TMPI << " EOL\n";
	cout << endl;

	cout << "What's a PhyDMG?" << endl;
	cin >> TMPI;
	file << "PHYDMG " << TMPI << " EOL\n";
	cout << endl;

	while (TMPS != "FINISH")
	{
		cout << "What's the Ability (FINISH to end)?" << endl;
		cin >> TMPS;
		if (TMPS == "FINISH") break;
		file << "ABILITY " << TMPS << " ";
		cout << endl;

		cout << "Any other param?" << endl;
		cin >> TMPI;
		file << "PARAM " << TMPI << " EOL\n";
		cout << endl;
	}

	cout << "Description? " << endl;
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);
	file << "DESCRIPTION " << TMPS << " EOL\n";
	
	file << "END";

	file.close();
}

void CObjectEditor::AddGate()
{
	string TMPS;
	string TMPS2;
	int TMPI;
	fstream file;

	file.open(filename, ios::app);

	system("cls");

	file << "\n\nGate\n";

	cout << "Enter destination: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "DESTINATION " << TMPS << " EOL\n";

	cout << endl << "Enter hard level: ";
	cin >> TMPS;
	TMPI = atoi(TMPS.data());

	file << "HARD LVL " << TMPI << " EOL\n";

	cout << endl << "Enter Dimension: ";
	cin >> TMPS;

	file << "DIMENSION " << TMPS << " EOL\n";




	file << "END";

	file.close();
}

void CObjectEditor::AddMythCard()
{
	string TMPS;
	string TMPS2;
	int TMPI;
	fstream file;

	file.open(filename, ios::app);

	system("cls");

	file << "\n\nMYTHCARD\n";

	cout << "Where will a gate open?: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "GATE " << TMPS << " EOL\n";

	cout << endl << "Where will clue appear?: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "CLUE " << TMPS << " EOL\n";


	while (true)
	{
		cout << endl << "Enter WhitePath: ";
		cin >> TMPS;
		if (TMPS == "END") break;
		file << "WHITEPATH " << TMPS << " EOL\n";
	}
	while (true)
	{
		cout << endl << "Enter BlackPath: ";
		cin >> TMPS;
		if (TMPS == "END") break;
		file << "BLACKPATH " << TMPS << " EOL\n";
	}

	file << "SCRIPT \n";
	while (true)
	{
		cin >> TMPS;
		getline(cin, TMPS2);
		TMPS.append(TMPS2);
		if (TMPS == "END") break;
		if (TMPS == "endl") file << "\n";
		else file << " " << TMPS;
	}
	file << "\nEND \n";


	file << "END";

	file.close();
}

void CObjectEditor::AddCharacter()
{
	string TMPS;
	string TMPS2;
	int TMPI;
	fstream file;

	file.open(filename, ios::app);

	system("cls");

	file << "\n\nCHARACTER\n";

	cout << "Name?: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "NAME " << TMPS << " EOL\n";

	cout << "Surname?: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "SURNAME " << TMPS << " EOL\n";

	cout << "Profession?: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "PROFESSION " << TMPS << " EOL\n";

	cout << "Max hp?: ";
	cin >> TMPS;

	file << "HP " << atoi(TMPS.data()) << " EOL\n";

	cout << "Max sanity?: ";
	cin >> TMPS;

	file << "SANITY " << atoi(TMPS.data()) << " EOL\n";

	cout << "Starting Location?: ";
	cin >> TMPS;
	getline(cin, TMPS2);
	TMPS.append(TMPS2);

	file << "STARTLOCATION " << TMPS << " EOL\n";

	///////////////////////////////////////////////////////
	cout << "Min Speed?: ";
	cin >> TMPS;

	file << "SPEED " << atoi(TMPS.data()) << " EOL\n";
	cout << "Min Sneak?: ";
	cin >> TMPS;

	file << "SNEAK " << atoi(TMPS.data()) << " EOL\n";
	///////////////////////////////////////////////////////
	cout << "Min fight?: ";
	cin >> TMPS;

	file << "FIGHT " << atoi(TMPS.data()) << " EOL\n";
	cout << "Min will?: ";
	cin >> TMPS;

	file << "WILL " << atoi(TMPS.data()) << " EOL\n";
	///////////////////////////////////////////////////////
	cout << "Min lore?: ";
	cin >> TMPS;

	file << "LORE " << atoi(TMPS.data()) << " EOL\n";
	cout << "Min Luck?: ";
	cin >> TMPS;

	file << "LUCK " << atoi(TMPS.data()) << " EOL\n";
	///////////////////////////////////////////////////////
	cout << "Focus?: ";
	cin >> TMPS;

	file << "FOCUS " << atoi(TMPS.data()) << " EOL\n";

	file << "END";

	file.close();
}

CObjectEditor::CObjectEditor()
{
}

CObjectEditor::~CObjectEditor()
{
}
