#include "MapStreet.h"
using namespace std;



bool CMapStreet::CheckIfMainPathInside(string name)
{
	bool result = false;

	for (int i = 0; i < maxMainLinkNumber; i++)
	{
		if (m_sMainLinks[i] == "Empty") break;
		if (m_sMainLinks[i] == name)
		{
			result = true;
			break;
		}
	}

	return result;
}

bool CMapStreet::CheckIfSidePathInside(string name)
{
	bool result = false;

	for (int i = 0; i < maxSideLinkNumber; i++)
	{
		if (m_sSideLinks[i] == "Empty") break;
		if (m_sSideLinks[i] == name)
		{
			result = true;
			break;
		}
	}

	return result;
}



CMapStreet::CMapStreet(string name)
{
	m_sBlackPath = "NONE";
	m_sWhitePath = "NONE";
	for (int i = 0; i < maxMainLinkNumber; i++)
	{
		m_sMainLinks[i] = "Empty";
	}
	for (int i = 0; i < maxSideLinkNumber; i++)
	{
		m_sSideLinks[i] = "Empty";
	}
	ChangeName(name);
}

CMapStreet::~CMapStreet()
{
}

bool CMapStreet::AddNewMainLink(string name)
{
	bool result = false;
	if (!CheckIfMainPathInside(name))
	{
		for (int i = 0; i < maxMainLinkNumber; i++)
		{
			if (m_sMainLinks[i] == "Empty")
			{
				m_sMainLinks[i] = name;
				result = true;
				break;
			}
		}
	}
	return result;
}

bool CMapStreet::AddNewSideLink(string name)
{
	bool result = false;
	if (!CheckIfSidePathInside(name))
	{
		for (int i = 0; i < maxSideLinkNumber; i++)
		{
			if (m_sSideLinks[i] == name) return true;
			if (m_sSideLinks[i] == "Empty")
			{
				m_sSideLinks[i] = name;
				result = true;
				break;
			}
		}
	}
	return result;
}

bool CMapStreet::ChangeWhitePath(string name)
{
	if (CheckIfMainPathInside(name))
	{
		m_sWhitePath = name;
		return true;
	}
	return false;
}

bool CMapStreet::ChangeBlackPath(string name)
{
	if (CheckIfMainPathInside(name))
	{
		m_sBlackPath = name;
		return true;
	}
	return false;
}
