#pragma once
#include "GameObject.h"
#include "AdditionalFunctionsAndStructures.h"


class CMonster :
	public CGameObject
{
private:
	int m_iAwareness;

	SMovement      m_sMovement;
	SDimensionType m_sDimension;
	SAbilities     m_sAbilities;

	string m_sDescription;

	int m_iSanityMod;
	int m_iSanityDMG;

	int m_iHealth;

	int m_iPhysicalMod;
	int m_iPhysicalDMG;
public:
	//Accessors
	inline int    GiveAwareness()   { return m_iAwareness; };
	char         *GiveMovement ();
	string        GiveDimention();
	SAbilities    GiveAbilities();
	inline string GiveDescription() { return m_sDescription; };
	inline int    GiveSanityMod()   { return m_iSanityMod; };
	inline int    GiveSanityDMG()   { return m_iSanityDMG; };
	inline int    GivePhysicalMod() { return m_iPhysicalMod; };
	inline int    GivePhysicalDMG() { return m_iPhysicalDMG; };
	inline int    GiveHealth()      { return m_iHealth; };
	//Modifiers 
	inline void ChangeAwereness(int newValue)     { m_iAwareness = newValue; };
	bool ChangeMovement        (string movementName);
	bool ChangeDimension       (string newDimension);
	bool ChangeAbilities       (string newAbility, int val = 0);
	bool ChangeDescription     (string newDesc);
	inline void ChangeSanity   (int mod, int dmg) { m_iSanityMod = mod; m_iSanityDMG = dmg; };
	inline void ChangePhysical (int mod, int dmg) { m_iPhysicalMod = mod; m_iPhysicalDMG = dmg; };
	inline void ChangeHealth   (int hp)           { m_iHealth = hp; };

	//Methods

	//C/D
	CMonster();
	~CMonster();
};

