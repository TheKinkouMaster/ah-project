#include "MapOutterWorld.h"



bool CMapOutterWorld::ChangeColor(const char color, bool wantedValue)
{
	if (color == 'G' || color == 'R' || color == 'Y' || color == 'B')
	{
		if (color == 'G') m_bIsGreen = wantedValue;
		if (color == 'R') m_bIsRed = wantedValue;
		if (color == 'B') m_bIsBlue = wantedValue;
		if (color == 'Y') m_bIsYellow = wantedValue;
		return true;
	}
	return false;
}



CMapOutterWorld::CMapOutterWorld(string name, bool red, bool blue, bool green, bool yellow)
{
	ChangeName(name);
	ChangeColor('R', red);
	ChangeColor('B', blue);
	ChangeColor('G', green);
	ChangeColor('Y', yellow);
}

CMapOutterWorld::~CMapOutterWorld()
{
}
