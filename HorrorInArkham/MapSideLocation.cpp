#include "MapSideLocation.h"



void CMapSideLocation::ChangeStreet(string newStreet)
{
	m_sStreetRegion = newStreet;
}

bool CMapSideLocation::ChangeResourceTrait(int which, int newTrait, bool certainity)
{
	if (which != 0 && which != 1)which = 0;
	if (newTrait > -1 && newTrait < 20)
	{
		m_iResourceTrait[which] = newTrait;
		m_bIsCertain[which] = certainity;
		return true;
	}
	return false;
}

bool CMapSideLocation::ChangeSafety(bool wantedSafety)
{
	m_bIsSafe = wantedSafety;
	return true;
}

bool CMapSideLocation::CheckStreetName(string name)
{
	if (m_sStreetRegion == name) return true;
	return false;
}


CMapSideLocation::CMapSideLocation(string name, string nameStreet, bool safetyness, int resourceTrait1, int resourceTrait2, bool cer1, bool cer2)
{
	ChangeName         (name);
	ChangeStreet       (nameStreet);
	ChangeResourceTrait(0, resourceTrait1, cer1);
	ChangeResourceTrait(1, resourceTrait2, cer2);
	ChangeSafety       (safetyness);
}

CMapSideLocation::~CMapSideLocation()
{
}
