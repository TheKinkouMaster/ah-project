#pragma once
#include <string>
#include "Constans.cpp"

using namespace std;
class CMapObject
{
private:
	string m_sName;
public:
	//Modifiers
	bool ChangeName(string name);

	//Accessors
	inline string GiveName() { return m_sName; };

	//Methods

	//C/D
	CMapObject();
	CMapObject(string name);
	~CMapObject();
};

