#include "MapEditorController.h"

bool CMapEditorController::MainEditorLoop()
{
	int itmp1 = 0;
	int itmp2 = 0;
	int itmp3 = 0;
	string stmp1;
	string stmp2;
	string stmp3;
	char ctmp1 = 'k';
 	while (itmp1 != 12)
	{
		system("cls");
		cout << "Main menu: " << endl;
		cout << "1. Add new street" << endl
			<< "2. Add new side location" << endl
			<< "3. Add new Outter world" << endl
			<< "4. Add new location" << endl
			<< "5. Modify Street" << endl
			<< "6. Modify Side location" << endl
			<< "7. Modify Outter location" << endl
			<< "8. Modify location" << endl
			<< "9. Sitrep" << endl
			<< "10. Save" << endl
			<< "11. Load" << endl
			<< "12. Exit" << endl;

		cin >> itmp1;

		switch (itmp1)
		{
		case 1:
			cin >> stmp1;
			getline(cin, stmp2);
 			AddNewStreet((stmp1+stmp2).data());
			break;
		case 2:
			cin >> stmp1;
			getline(cin, stmp2);
			AddNewSideLocation((stmp1 + stmp2).data());
			break;
		case 3:
			cin >> stmp1;
			getline(cin, stmp2);
			AddNewOutterLocation((stmp1 + stmp2).data());
			break;
		case 4:
			cin >> stmp1;
			getline(cin, stmp2);
			AddnewLocation((stmp1 + stmp2).data());
			break;
		case 5:
			system("cls");
			WOCStreets();
			cout << "Which street?: ";
			cin >> stmp1;
			getline(cin, stmp2);
			stmp1 = (stmp1 + stmp2);

			while (true)
			{
				system("cls");
				WOCStreet(stmp1.data());
				cout << "What would u like to do? " << endl
					<< "1. Change black path" << endl
					<< "2. Change white path" << endl
					<< "3. Change name" << endl
					<< "4. Add street link" << endl
					<< "5. Add side location link" << endl
					<< "6. Exit" << endl;
				cin >> itmp2;
				switch (itmp2)
				{
				case 1:
					cout << "name of new path: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPStreets[returnPosition(stmp1.data())]->ChangeBlackPath((stmp2 + stmp3).data());
					break;
				case 2:
					cout << "name of new path: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPStreets[returnPosition(stmp1.data())]->ChangeWhitePath((stmp2 + stmp3).data());
					break;
				case 3:
					cout << "new name: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPStreets[returnPosition(stmp1.data())]->ChangeName((stmp2+stmp3).data());
					break;
				case 4:
					cout << "name of new path: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPStreets[returnPosition(stmp1.data())]->AddNewMainLink((stmp2 + stmp3).data());
					m_CMAPStreets[returnPosition((stmp2 + stmp3).data())]->AddNewMainLink(stmp1.data());
					break;
				case 5:
					cout << "name of new path: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPStreets[returnPosition(stmp1.data())]->AddNewSideLink((stmp2 + stmp3).data());
					m_CMAPSideLocations[returnPosition((stmp2 + stmp3).data())]->ChangeStreet((stmp1).data());
					break;
				default:
					break;
				}
				if (itmp2 == 6) break;
			}

			
			break;
		case 6:
			system("cls");
			WOCSideLocations();
			cout << "Which location?: ";
			cin >> stmp1;
			getline(cin, stmp2);
			stmp1 = (stmp1 + stmp2);

			while (true)
			{
				system("cls");
				WOCSideLocation(stmp1.data());
				cout << "What would u like to do? " << endl
					<< "1. Change street" << endl
					<< "2. Change safety" << endl
					<< "3. Change name" << endl
					<< "4. Change resource trait" << endl
					<< "5. Exit" << endl;
				cin >> itmp2;
				switch (itmp2)
				{
				case 1:
					cout << "name of new path: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPSideLocations[returnPosition(stmp1.data())]->ChangeStreet((stmp2 + stmp3).data());
					m_CMAPStreets[returnPosition((stmp2+stmp3).data())]->AddNewSideLink((stmp1).data());
					break;
				case 2:
					if(m_CMAPSideLocations[returnPosition(stmp1.data())]->GiveIsSafe()==true)m_CMAPSideLocations[returnPosition(stmp1.data())]->ChangeSafety(false);
					else m_CMAPSideLocations[returnPosition(stmp1.data())]->ChangeSafety(true);
					break;
				case 3:
					cout << "new name: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPSideLocations[returnPosition(stmp1.data())]->ChangeName((stmp2+stmp3).data());
					break;
				case 4:
					cout << "Which?" << endl;
					cin >> itmp1;
					cout << "to what?" << endl;
					cin >> itmp2;
					cout << "certainity?" << endl;
					cin >> itmp3;
					if(itmp3 == 0)m_CMAPSideLocations[returnPosition(stmp1.data())]->ChangeResourceTrait(itmp1, itmp2, false);
					else m_CMAPSideLocations[returnPosition(stmp1.data())]->ChangeResourceTrait(itmp1, itmp2, true);
					break;
				default:
					break;
				}
				if (itmp2 == 5) break;
			}
			break;
		case 7:
			system("cls");
			WOCOutterWorld();
			cout << "which location? " << endl;
			cin >> stmp2;
			getline(cin, stmp3);
			stmp1 = stmp2 + stmp3;
			while (true)
			{
				system("cls");
				WOCOutterWorld(stmp1.data());
				cout << "What would u like to do? " << endl
					<< "1. Change color" << endl
					<< "2. Change name" << endl
					<< "3. Exit" << endl;
				cin >> itmp2;
				switch (itmp2)
				{
				case 1:
					cout << "write first letter of a color in capital: ";
					cin >> ctmp1;
					cout << endl << "true or false? " << endl;
					cin >> itmp3;

					if (itmp3 == 0)m_CMAPOutterLocations[returnPosition(stmp1.data())]->ChangeColor(ctmp1, false);
					else m_CMAPOutterLocations[returnPosition(stmp1.data())]->ChangeColor(ctmp1, true);
					break;
				case 2:
					cout << "new name: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPOutterLocations[returnPosition(stmp1.data())]->ChangeName((stmp2 + stmp3).data());
					break;
				default:
					break;
				}
				if (itmp2 == 3) break;
			}

			break;
		case 8:
			system("cls");
			WOCLocations();
			cout << "which location? " << endl;
			cin >> stmp2;
			getline(cin, stmp3);
			stmp1 = stmp2 + stmp3;
			while (true)
			{
				system("cls");
				WOCOutterWorld(stmp1.data());
				cout << "What would u like to do? " << endl
					<< "1. Change name" << endl
					<< "2. Exit" << endl;
				cin >> itmp2;
				switch (itmp2)
				{
				case 1:
					cout << "new name: ";
					cin >> stmp2;
					getline(cin, stmp3);
					m_CMAPLocations[returnPosition(stmp1.data())]->ChangeName((stmp2 + stmp3).data());
					break;
				default:
					break;
				}
				if (itmp2 == 3) break;
			}
			break;
		case 9:
			WOCStreets();
			WOCSideLocations();
			WOCLocations();
			WOCOutterWorld();
			system("PAUSE");
			break;
		case 10:
			system("cls");
			cout << "filename?";
			cin >> stmp1;
			SaveMapToFIle(stmp1.data());
			break;
		case 11:
			system("cls");
			cout << "filename?";
			cin >> stmp1;
			OpenMapFromFile(stmp1.data());
			break;
		default:
			break;
		}
	}
	return false;
}

CMapEditorController::CMapEditorController()
{
	for (int i = 0; i < maxMapLocations; i++)
	{
		m_CMAPLocations[i] = NULL;
	}
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		m_CMAPSideLocations[i] = NULL;
	}
	for (int i = 0; i < maxMapOutterLocations; i++)
	{
		m_CMAPOutterLocations[i] = NULL;
	}
	for (int i = 0; i < maxMapStreets; i++)
	{
		m_CMAPStreets[i] = NULL;
	}
	for (int i = 0; i < 11; i++)
	{
		panicTrait[i] = NULL;
	}
}

CMapEditorController::~CMapEditorController()
{
}

bool CMapEditorController::WOCStreets()
{
	for (int i = 0; i < maxMapStreets; i++)
	{
		if (m_CMAPStreets[i] != NULL)cout << m_CMAPStreets[i]->GiveName() << endl;
	}
	return true;
}

bool CMapEditorController::WOCSideLocations()
{
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		if (m_CMAPSideLocations[i] != NULL)cout << m_CMAPSideLocations[i]->GiveName() << endl;
	}
	return true;
}

bool CMapEditorController::WOCLocations()
{
	for (int i = 0; i < maxMapLocations; i++)
	{
		if (m_CMAPLocations[i] != NULL)cout << m_CMAPLocations[i]->GiveName() << endl;
	}
	return true;
}

bool CMapEditorController::WOCOutterWorld()
{
	for (int i = 0; i < maxMapOutterLocations; i++)
	{
		if (m_CMAPOutterLocations[i] != NULL)cout << m_CMAPOutterLocations[i]->GiveName() << endl;
	}
	return true;
}

bool CMapEditorController::WOCStreet(const char * streetName)
{
	cout << m_CMAPStreets[returnPosition(streetName)]->GiveName() << ": " << endl;
	cout << "Main links: " << endl;
	for (int i = 0; i < maxMainLinkNumber; i++)
	{
		if (m_CMAPStreets[returnPosition(streetName)]->GiveMainLinks(i).length() < 2)break;
		cout << m_CMAPStreets[returnPosition(streetName)]->GiveMainLinks(i) << endl;
	}
	cout << endl << "Black Path: " << m_CMAPStreets[returnPosition(streetName)]->GiveBlackPath() << endl
		<< "White path: " << m_CMAPStreets[returnPosition(streetName)]->GiveWhitePath() << endl;
	cout << "Side Links: " << endl;
	for (int i = 0; i < maxSideLinkNumber; i++)
	{
		if (m_CMAPStreets[returnPosition(streetName)]->GiveSideLinks(i).length() < 2)break;
		cout << m_CMAPStreets[returnPosition(streetName)]->GiveSideLinks(i) << endl;
	}
	return true;
}

bool CMapEditorController::WOCSideLocation(const char * sideLocation)
{
	cout << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveName() << ": " << endl;
	cout << "Linked to: " << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveStreet() << endl;
	cout << "Safety: " << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveIsSafe() << endl;
	cout << "Resource trait  :" << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveResourceTrait(0) << " " << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveIsCertain(0) << endl;
	cout << "Resource trait 2:" << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveResourceTrait(1) << " " << m_CMAPSideLocations[returnPosition(sideLocation)]->GiveIsCertain(1) << endl;

	return true;
}

bool CMapEditorController::WOCOutterWorld(const char * worldName)
{
	cout << m_CMAPOutterLocations[returnPosition(worldName)]->GiveName() << ": " << endl;
	if (m_CMAPOutterLocations[returnPosition(worldName)]->isBlue() == 1)cout << "Is blue" << endl;
	if (m_CMAPOutterLocations[returnPosition(worldName)]->isGreen() == 1)cout << "Is green" << endl;
	if (m_CMAPOutterLocations[returnPosition(worldName)]->isRed() == 1)cout << "Is red" << endl;
	if (m_CMAPOutterLocations[returnPosition(worldName)]->isYellow() == 1)cout << "Is yellow" << endl;
	return false;
}
