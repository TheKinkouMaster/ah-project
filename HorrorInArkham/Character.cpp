#include "Character.h"



CCharacter::CCharacter()
{
	ChangeName            ("Default");
	ChangeLocation        ("Default");
	ChangeProfession      ("Default");
	ChangeStartingPosition("Default");
	ChangeSurname         ("Default");

	for (int i = 0; i < 20; i++)
	{
		m_rHealth[i]          = NULL;
		m_rSanity[i]          = NULL;
		m_rClues[i]           = NULL;
		m_rMonsterTrophies[i] = NULL;
	}

	m_iFocus           = 0;
	m_iFight           = 0;
	m_iFirstIndicator  = 0;
	m_iLore            = 0;
	m_iLuck            = 0;
	m_iSecondIndicator = 0;
	m_iSneak           = 0;
	m_iSpeed           = 0;
	m_iThirdIndicator  = 0;
	m_iWill            = 0;
	m_bIsOnFirstOL     = true;

	SetMaxHP    (0);
	SetMaxSanity(0);
}


CCharacter::~CCharacter()
{
	for (int i = 0; i < 20; i++)
	{
		if (m_rHealth[i]          != NULL) delete(m_rHealth[i]);
		if (m_rSanity[i]          != NULL) delete(m_rSanity[i]);
		if (m_rMonsterTrophies[i] != NULL) delete(m_rMonsterTrophies[i]);
	}
}

int CCharacter::GiveHp()
{
	for (int i = 0; i < 20; i++)
		if (m_rHealth[i] == NULL)return i;
	return 0;
}

int CCharacter::GiveSanity()
{
	for (int i = 0; i < 20; i++)
		if (m_rSanity[i] == NULL)return i;
	return 0;
}

int CCharacter::GiveClues()
{
	for (int i = 0; i < 20; i++)
		if (m_rClues[i] == NULL)return i;
	return 0;
}

int CCharacter::GiveMonsters()
{
	int counter = 0;
	for (int i = 0; i <= 20; i++)
		if (m_rMonsterTrophies[i] != NULL) counter++;
	return counter;
}

bool CCharacter::MoveIndicator(int which, bool toWhatSide)
{
	int value = 0;
	if (toWhatSide == false) value--;
	if (toWhatSide == true) value++;
	if (which == 1)
	{
		m_iFirstIndicator = m_iFirstIndicator + value;
		if (m_iFirstIndicator < 0)
		{
			m_iFirstIndicator = 0;
			return false;
		}
		if (m_iFirstIndicator > 3)
		{
			m_iFirstIndicator = 3;
			return false;
		}
		return true;
	}
	if (which == 2)
	{
		m_iSecondIndicator = m_iSecondIndicator + value;
		if (m_iSecondIndicator < 0)
		{
			m_iSecondIndicator = 0;
			return false;
		}
		if (m_iSecondIndicator > 3)
		{
			m_iSecondIndicator = 3;
			return false;
		}
		return true;
	}
	if (which == 3)
	{
		m_iThirdIndicator = m_iThirdIndicator + value;
		if (m_iThirdIndicator < 0)
		{
			m_iThirdIndicator = 0;
			return false;
		}
		if (m_iThirdIndicator > 3)
		{
			m_iThirdIndicator = 3;
			return false;
		}
		return true;
	}
	return false;
}

void CCharacter::SetFirstStats(int speed, int sneak)
{
	m_iSpeed = speed;
	m_iSneak = sneak;
}

void CCharacter::SetSecondsStats(int fight, int will)
{
	m_iFight = fight;
	m_iWill = will;
}

void CCharacter::SetThirdStats(int lore, int luck)
{
	m_iLore = lore;
	m_iLuck = luck;
}

void CCharacter::AddHealthToken(CToken * newToken)
{
	for (int i = 0; i < 20; i++)
	{
		if (m_rHealth[i] == NULL)
		{
			m_rHealth[i] = newToken;
			break;
		}
	}
}

void CCharacter::AddSanityToken(CToken * newToken)
{
	for (int i = 0; i < 20; i++)
	{
		if (m_rSanity[i] == NULL)
		{
			m_rSanity[i] = newToken;
			break;
		}
	}
}

void CCharacter::AddClueToken(CToken * newToken)
{
	for (int i = 0; i < 20; i++)
	{
		if (m_rClues[i] == NULL)
		{
			m_rClues[i] = newToken;
			break;
		}
	}
}

CToken * CCharacter::RemoveHealthToken()
{
	CToken *ptr = nullptr;
	for (int i = 0; i < 20; i++)
	{
		if (i == 0 && m_rHealth[i] == NULL)return nullptr;
		if (m_rHealth[i + 1] == NULL)
		{
			ptr = m_rHealth[i];
			m_rHealth[i] = nullptr;
			return ptr;
		}
	}
	return nullptr;
}

CToken * CCharacter::RemoveSanityToken()
{
	CToken *ptr = nullptr;
	for (int i = 0; i < 20; i++)
	{
		if (i == 0 && m_rSanity[i] == NULL)return nullptr;
		if (m_rSanity[i + 1] == NULL)
		{
			ptr = m_rSanity[i];
			m_rSanity[i] = nullptr;
			return ptr;
		}
	}
	return nullptr;
}

CToken * CCharacter::RemoveClueToken()
{
	CToken *ptr = nullptr;
	for (int i = 0; i < 20; i++)
	{
		if (i == 0 && m_rClues[i] == NULL)return nullptr;
		if (m_rClues[i + 1] == NULL)
		{
			ptr = m_rClues[i];
			m_rClues[i] = nullptr;
			return ptr;
		}
	}
	return nullptr;
}

bool CCharacter::AddMonsterTrophy(CMonster * ptr)
{
	for (int i = 0; i < 20; i++)
	{
		if (m_rMonsterTrophies[i] == NULL)
		{
			m_rMonsterTrophies[i] = ptr;
			ptr->ChangeLocation(m_sSurname);
			break;
		}
	}
	return true;
}
