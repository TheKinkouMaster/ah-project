#pragma once
#include <iostream>
#include <Windows.h>
#include "Editor.h"
#include "MyMap.h"
#include "Resource.h"
#include "EventController.h"
#include "AdditionalFunctionsAndStructures.h"
#include <conio.h>

using namespace std;

class CGameController
{
private:
	CEditor          EditorUnit;
	CEventController EventControllerUnit;

	CMyMap    *m_RMap;
	CResource *m_RBox;
	CResource *m_RInGame;

	GameWorldModifiers m_SWorldSpaceModifiers;
public:
	//Accessors
	bool isMapSet();
	bool isBoxSet();
	bool isInGameSet();
	//Modifiers
	bool SetupMap();
	bool SetupBox();
	bool SetupGameWorld();
	bool SetupPlayer();
	bool SetupAncient();

	bool DisplayPlayerStats(int which);
	bool StatsChanging     (int which, bool setup);
	

	bool Phasing();

	bool FifthPhase();
	bool FirstPhase();
	bool SecondPhase();
	bool ThirdPhase();
	bool FourthPhase();

	//Methods
	bool MainControllerLoop();
	//C/S
	CGameController();
	~CGameController();
};

