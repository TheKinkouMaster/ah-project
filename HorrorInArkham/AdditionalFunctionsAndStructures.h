#pragma once
#include "MyEvent.h"

struct SMovement
{
	bool isNormal;
	bool isFast;
	bool isFlying;
	bool isStatic;
	bool isSpecial;
};
struct SDimensionType
{
	bool triangle;
	bool cross;
	bool line;
	bool square;
	bool diamond;
	bool circle;
	bool hexagon;
	bool star;
	bool moon;
};
struct SAbilities
{
	bool ambush;
	bool ethernal;
	bool magicResistans;
	bool physicalResistans;
	bool magicInsensitivity;
	bool physicalInsensivitivity;
	bool nightmare;
	int nightmareValue;
	bool overwhelming;
	int overwhelmingValue;
	bool mask;
	bool undead;
};
struct GameWorldModifiers
{

};

static int GlobalCounter = 1;
static int GlobalCounter2 = 1;
static int GlobalTurnCounter = 0;

int roleDice(int howManyTimes, string mode);
