#include "CardMyth.h"



bool CCardMyth::ChangeClueLocation(string newLocation)
{
	m_sClueLocation = newLocation;
	return true;
}

bool CCardMyth::ChangeGateLocation(string newLocation)
{
	m_sGateLocation = newLocation;
	return true;
}

bool CCardMyth::AddWhiteMovement(string sign)
{
	if      (sign == "Circle")  m_sWhite.circle   = true;
	else if (sign == "Cross")   m_sWhite.cross    = true;
	else if (sign == "Diamond") m_sWhite.diamond  = true;
	else if (sign == "Hexagon") m_sWhite.hexagon  = true;
	else if (sign == "Line")    m_sWhite.line     = true;
	else if (sign == "Moon")    m_sWhite.moon     = true;
	else if (sign == "Square")  m_sWhite.square   = true;
	else if (sign == "Star")    m_sWhite.star     = true;
	else if (sign == "Triangle")m_sWhite.triangle = true;
	else return false;
	return true;
}

bool CCardMyth::AddBlackMovement(string sign)
{
	if      (sign == "Circle")  m_sBlack.circle   = true;
	else if (sign == "Cross")   m_sBlack.cross    = true;
	else if (sign == "Diamond") m_sBlack.diamond  = true;
	else if (sign == "Hexagon") m_sBlack.hexagon  = true;
	else if (sign == "Line")    m_sBlack.line     = true;
	else if (sign == "Moon")    m_sBlack.moon     = true;
	else if (sign == "Square")  m_sBlack.square   = true;
	else if (sign == "Star")    m_sBlack.star     = true;
	else if (sign == "Triangle")m_sBlack.triangle = true;
	else return false;
	return true;
}

bool CCardMyth::ChangeScript(CMyEvent newScript)
{
	m_sScripts = newScript;
	return true;
}

CCardMyth::CCardMyth()
{
	m_sBlack.circle   = false;
	m_sBlack.cross    = false;
	m_sBlack.diamond  = false;
	m_sBlack.hexagon  = false;
	m_sBlack.line     = false;
	m_sBlack.moon     = false;
	m_sBlack.square   = false;
	m_sBlack.star     = false;
	m_sBlack.triangle = false;

	m_sWhite.circle   = false;
	m_sWhite.cross    = false;
	m_sWhite.diamond  = false;
	m_sWhite.hexagon  = false;
	m_sWhite.line     = false;
	m_sWhite.moon     = false;
	m_sWhite.square   = false;
	m_sWhite.star     = false;
	m_sWhite.triangle = false;

	m_sClueLocation = "NONE";
	m_sGateLocation = "NONE";

	ChangeLocation("BOX");
	ChangeName    ("MythCard");
}


CCardMyth::~CCardMyth()
{
}
