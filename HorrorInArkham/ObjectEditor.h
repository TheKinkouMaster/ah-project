#pragma once
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class CObjectEditor
{
private:
	string filename;
public:
	//Accessors
	string GiveFilename() { return filename; };

	//Modifiers
	inline void ChangeName(string name) { filename = name; };

	//Methods
	void MainEditorLoop();
	void AddMonster    ();
	void AddGate       ();
	void AddMythCard   ();
	void AddCharacter  ();

	//C/D
	CObjectEditor ();
	~CObjectEditor();
};

