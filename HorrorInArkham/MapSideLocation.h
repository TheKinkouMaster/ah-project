#pragma once
#include "MapObject.h"
class CMapSideLocation :
	public CMapObject
{
private:
	string m_sStreetRegion;

	bool m_bIsSafe;

	int m_iResourceTrait[2]; //See MarkerDocumentation.txt
	bool m_bIsCertain[2]; //Certainity for each reasource
public:
	//Accessors
	inline string GiveStreet()                  { return m_sStreetRegion; };
	inline bool GiveIsSafe()                    { return m_bIsSafe; };
	inline bool GiveIsCertain   (int which = 0) { if (which == 0 || which == 1)return m_bIsCertain[which];     return false; };
	inline int GiveResourceTrait(int which = 0) { if (which == 0 || which == 1)return m_iResourceTrait[which]; return false; };

	//Modifiers
	void ChangeStreet       (string newStreet);
	bool ChangeResourceTrait(int which = 0, int newTrait = 0, bool certainity = false);
	bool ChangeSafety       (bool wantedSafety = false);

	//Methods
	bool CheckStreetName    (string name);

	//Constructors/destructors
	CMapSideLocation        (string = "default_location", string nameStreet = "default_location", bool safetyness = false,
		                     int resourceTrait1 = 0, int resourceTrait2 = 0, bool cer1 = false, bool cer2 = false);
	~CMapSideLocation();
};

