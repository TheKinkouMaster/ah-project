#pragma once
#include "MapObject.h"

using namespace std;
class CMapStreet :
	public CMapObject
{
private:
	string m_sMainLinks[maxMainLinkNumber];
	string m_sSideLinks[maxSideLinkNumber];

	string m_sWhitePath;
	string m_sBlackPath;
public:
	//Accessors
	inline string GiveMainLinks(int which) { return m_sMainLinks[which]; };
	inline string GiveSideLinks(int which) { return m_sSideLinks[which]; };
	inline string GiveWhitePath()          { return m_sWhitePath; };
	inline string GiveBlackPath()          { return m_sBlackPath; };

	//Modifiers
	bool AddNewMainLink (string name);
	bool AddNewSideLink (string name);
	bool ChangeWhitePath(string name);
	bool ChangeBlackPath(string name);

	//Checkers
	bool CheckIfMainPathInside(string name);
	bool CheckIfSidePathInside(string name);

	//C/D
	CMapStreet (string name = NULL);
	~CMapStreet();
};

