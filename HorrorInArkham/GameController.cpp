#include "GameController.h"



bool CGameController::MainControllerLoop()
{
	string answerS;
	int answer = 0;
	while (answer != 10)
	{
		system("cls");
		cout << "Welcome to Horror in Arkham!" << endl;
		cout << "1. Editor" << endl;
		cout << "2. New game" << endl;
		cout << "3. Exit" << endl;

		cin >> answerS;
		answer = atoi(answerS.data());

		switch (answer)
		{
		case 1:
			EditorUnit.mainEditorLoop();
			break;
		case 2:
			if (SetupMap() == false) { cout << "Something went wrong..." << endl; break; };
			if (SetupBox() == false) { cout << "Something went wrong..." << endl; break; };
			if (SetupGameWorld() == false) { cout << "Something went wrong..." << endl; break; };
			if (SetupAncient() == false) { cout << "Something went wrong..." << endl; break; };
			if (SetupPlayer() == false) { cout << "Something went wrong..." << endl; break; };
			FifthPhase();
			Phasing();
			delete(m_RBox);
			m_RBox = NULL;
			delete(m_RInGame);
			m_RInGame = NULL;
			delete(m_RMap);
			m_RMap = NULL;
			break;
		case 3:
			answer = 10;
			break;
		default:
			break;
		}
	}
	return true;
}


CGameController::CGameController()
{
	m_RBox = NULL;
	m_RInGame = NULL;
	m_RMap = NULL;
}


CGameController::~CGameController()
{
}

bool CGameController::isMapSet()
{
	if (m_RMap == NULL)return false;
	return true;
}

bool CGameController::isBoxSet()
{
	if (m_RBox == NULL)return false;
	return true;
}

bool CGameController::isInGameSet()
{
	if (m_RInGame == NULL)return false;
	return true;
}

bool CGameController::SetupMap()
{
	string choiceS;
	fstream file;

	system("CLS");

	cout << "Please enter the name of map file" << endl;
	cin >> choiceS;

	file.open(choiceS);
	while (true)
	{
		if (file.good())
		{
			cout << "Access to file granted! Creating map";
			file.close();
			Sleep(2000);
			m_RMap = new CMyMap();
			m_RMap->OpenMapFromFile(choiceS.data());
			return true;
		}
		system("CLS");
		cout << endl << "Wrong file!" << endl;
		cout << "Enter name once again or write EXIT to... exit" << endl;
		cin >> choiceS;

		if (choiceS == "EXIT") break;
		file.open(choiceS);
	}
	return false;
}

bool CGameController::SetupBox()
{
	string choiceS;
	fstream file;

	system("CLS");

	cout << "Please enter the name of box file" << endl;
	cin >> choiceS;

	file.open(choiceS);
	while (true)
	{
		if (file.good())
		{
			cout << "Access to file granted! Creating box";
			file.close();
			Sleep(2000);
			m_RBox = new CResource();
			m_RBox->ImportData(choiceS.data());
			cout << endl << "Box has been created. Setting up game world." << endl;
			m_RInGame = new CResource();
			Sleep(2000);
			return true;
		}
		system("CLS");
		cout << endl << "Wrong file!" << endl;
		cout << "Enter name once again or write EXIT to... exit" << endl;
		cin >> choiceS;

		if (choiceS == "EXIT") break;
		file.open(choiceS);
	}
	return false;
}

bool CGameController::SetupGameWorld()
{
	CMyEvent SettingUpClues;
	CMyEvent *ptr = &SettingUpClues;
	EventControllerUnit.SetGame(m_RInGame);
	EventControllerUnit.SetMap(m_RMap);
	EventControllerUnit.SetPack(m_RBox);
	{
		ptr->AddEventCommnand("GTAL");
		ptr->AddEventCommnand("SIDELOCATION");
		ptr->AddEventCommnand("SAFETY");
		ptr->AddEventCommnand("FALSE");

		ptr = ptr->CreateNextEvent();

		ptr->AddEventCommnand("SPAWN");
		ptr->AddEventCommnand("CLUE");
		ptr->AddEventCommnand("ARRAY");
		ptr->AddEventCommnand("1");
	}
	EventControllerUnit.CallEvent(SettingUpClues);
	return true;
}

bool CGameController::SetupPlayer()
{
	string command;
	bool exit = false;
	system("CLS");
	cout << "You can hear screaming.... Terrible whistles all around you... \nand silent whispers within you." << endl
		<< "Dark light passess through you closed eyes. \nPainful relieve walks through you soul" << endl
		<< "You try to remember your own name... What is it?" << endl;
	for (int i = 0; i < 20; i++)
	{
		if (m_RBox->GiveCharacterPtr(i) == nullptr) break;
		cout << m_RBox->GiveCharacterPtr(i)->GiveName() << " " << m_RBox->GiveCharacterPtr(i)->GiveSurname() << endl;
	}
	while (exit == false)
	{
		cout << endl << "Enter surname: ";
		cin >> command;

		for (int i = 0; i < 20; i++)
		{
			if (m_RBox->GiveCharacterPtr(i) == nullptr) break;
			if (m_RBox->GiveCharacterPtr(i)->GiveSurname() == command)
			{
				m_RInGame->AddCharacter(m_RBox->GiveCharacter(i));
				exit = true;
				break;
			}
		}
	}
	for (int i = 0; i < m_RInGame->GiveCharacterPtr()->GiveMaxHP(); i++)
		m_RInGame->GiveCharacterPtr()->AddHealthToken(m_RBox->GiveToken("HEALTH"));
	for (int i = 0; i < m_RInGame->GiveCharacterPtr()->GiveMaxSanity(); i++)
		m_RInGame->GiveCharacterPtr()->AddSanityToken(m_RBox->GiveToken("SANITY"));

	return true;
}

bool CGameController::SetupAncient()
{
	return true;
}

bool CGameController::DisplayPlayerStats(int which)
{
	CCharacter *ptr;
	if (m_RInGame->GiveCharacterPtr(which) == nullptr)return false;
	ptr = m_RInGame->GiveCharacterPtr(which);

	cout << ptr->GiveName() << " " << ptr->GiveSurname() << "." << endl;
	cout << "HP: " << ptr->GiveHp() << "/" << ptr->GiveMaxHP() << endl;
	cout << "Sanity: " << ptr->GiveSanity() << "/" << ptr->GiveMaxSanity() << endl;
	cout << "Clues: " << ptr->GiveClues() << endl;
	cout << "Monster trophies: " << ptr->GiveMonsters() << endl;

	cout << endl;

	cout << "Speed " << ptr->GiveSpeed() << endl;
	cout << "Sneak " << ptr->GiveSneak() << endl;

	cout << "Fight " << ptr->GiveFight() << endl;
	cout << "Will " << ptr->GiveWill() << endl;

	cout << "Lore " << ptr->GiveLore() << endl;
	cout << "Luck " << ptr->GiveLuck() << endl;

	return true;
}

bool CGameController::StatsChanging(int which, bool setup)
{
	CCharacter *ptr = m_RInGame->GiveCharacterPtr(which);
	int choice;
	string choiceStr;
	bool exit = false;
	bool IncOrDec = false;
	int focus = ptr->GiveFocus();

	while (exit == false)
	{
		system("cls");

		cout << "Speed " << ptr->GiveSpeed() << endl;
		cout << "Sneak " << ptr->GiveSneak() << endl;
		 
		cout << "Fight " << ptr->GiveFight() << endl;
		cout << "Will " << ptr->GiveWill() << endl;
		 
		cout << "Lore " << ptr->GiveLore() << endl;
		cout << "Luck " << ptr->GiveLuck() << endl;

		if (setup == false) cout << endl << "Focus points: " << ptr->GiveFocus() << endl;;

		cout << "Which stats change? (1-3, 4 - Exit)?: ";
		cin >> choiceStr;
		choice = atoi(choiceStr.data());

		switch (choice)
		{
		case 1:
			cout << endl << "Increace or decreace? 1/0" << endl;
			cin >> choiceStr;
			choice = atoi(choiceStr.data());

			if (choice == 0) IncOrDec = false;
			else IncOrDec = true;

			if (ptr->MoveIndicator(1, IncOrDec) == false)
				cout << endl << "You cannot increace you stats any further." << endl;
			focus--;
			break;

		case 2:
			cout << endl << "Increace or decreace? 1/0" << endl;
			cin >> choiceStr;
			choice = atoi(choiceStr.data());

			if (choice == 0) IncOrDec = false;
			else IncOrDec = true;
			if (ptr->MoveIndicator(2, IncOrDec) == false)
				cout << endl << "You cannot increace you stats any further." << endl;
			focus--;
			break;
		case 3:
			cout << endl << "Increace or decreace? 1/0" << endl;
			cin >> choiceStr;
			choice = atoi(choiceStr.data());

			if (choice == 0) IncOrDec = false;
			else IncOrDec = true;
			if (ptr->MoveIndicator(3, IncOrDec) == false)
				cout << endl << "You cannot increace you stats any further." << endl;
			focus--;
			break;
		case 4:
			exit = true;
			break;
		case 5:
			cout << "Unknown command" << endl;
			_getch();
			break;
		}
		if (focus == 0 && setup == false) exit = true;
	}
	return true;
}

bool CGameController::Phasing()
{
	extern bool exit_current_game;
	while (GlobalTurnCounter < 20 || exit_current_game == true) 
	{
		exit_current_game = false;
		FirstPhase();
		if (exit_current_game == true) break;
		SecondPhase();
		if (exit_current_game == true) break;
		ThirdPhase();
		if (exit_current_game == true) break;
		FourthPhase();
		if (exit_current_game == true) break;
		FifthPhase();
		if (exit_current_game == true) break;
	}
	system("cls");
	if (m_RInGame->GiveCharacterPtr(0)->GiveHp() == 0 || m_RInGame->GiveCharacterPtr(0)->GiveSanity() == 0)
		cout << "You lost the game... Try again!";
		
	else
		cout << "Gratz! You survived 20 turns." << endl
		<< "Military swept through city and brand back peace" << endl;
	_getch();
	return true;
}

bool CGameController::FifthPhase()
{
	CCardMyth *ptr = NULL;

	GlobalTurnCounter++;

	system("CLS");

	//Picking Card
	ptr = m_RBox->GiveMythCard();

	cout << "The end approches." << endl;

	EventControllerUnit.CallMythCard(ptr);

	return false;
}

bool CGameController::FirstPhase()
{
	system("cls");
	int choice;
	string choiceStr;
	extern bool exit_current_game;

	cout << "Dawn is near. Prepare yourself!" << endl << "Phase 1... " << endl << endl;

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_RInGame->GiveCharacterPtr(i) == NULL) break;
		DisplayPlayerStats(i);

		cout << "1. Resetup skills" << endl;
		cout << "2. Move on" << endl;
		cout << "3. Exit game" << endl;

		cin >> choiceStr;
		cin.clear();
		choice = atoi(choiceStr.data());

		switch (choice)
		{
		case 1:
			if (GlobalTurnCounter == 1) StatsChanging(i, true);
			else StatsChanging(i, false);
			break;
		case 3:
			exit_current_game = true;
			break;
		default:
			break;
		}
		
	}

	return false;
}

bool CGameController::SecondPhase()
{
	int movementPoints = 0;
	int choice = 0;
	int tmpInt1 = 0;
	int tmpInt2 = 0;

	extern int GlobalCounter;
	CMyEvent *EventPtr = NULL;
	CMyEvent *EventPtrRoot = NULL;
	CToken *TokenPtr = NULL;

	int counter = 0;

	string strArray[100];

	system("Cls");
	cout << "When the sun breaks through black clouds... to Arkham comes Second Phase..." << endl;
	_getch();
	{ 
		for (int i = 0; i < MAXRESOURCENUMBER; i ++ )
		{
			if (m_RInGame->GiveCharacterPtr(i) == NULL)break;
			movementPoints = m_RInGame->GiveCharacterPtr(i)->GiveSpeed();
			if (m_RMap->CheckIfTheresOutterWorld(m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data()) == true)
			{
				if (m_RInGame->GiveCharacterPtr(i)->GiveIsOnFirstOL() == true)
				{
					cout << "You moved to next stage of outter world!" << endl;
					_getch();
					m_RInGame->GiveCharacterPtr(i)->ChangeOLStage(false);
				}//player is on the first position
				else // player is on second position
				{
					for (int j = 0; j < MAXRESOURCENUMBER; j++)
					{
						if (m_RInGame->GiveGatePtr(j) == NULL)
						{
							cout << "You have lost in time and space!" << endl;
							_getch();
							m_RInGame->GiveCharacterPtr(i)->ChangeLocation("Lost in Time&Space");
							break;
						}
						if (m_RInGame->GiveGatePtr(j)->GiveDestination() == m_RInGame->GiveCharacterPtr(i)->GiveLocationName())
						{
							cout << "You moved back into Arkham!" << endl;
							_getch();
							m_RInGame->GiveCharacterPtr(i)->ChangeLocation(m_RInGame->GiveGatePtr(j)->GiveLocationName());
							TokenPtr = m_RBox->GiveToken("SEALSIGN");
							TokenPtr->ChangeLocation(m_RInGame->GiveGatePtr(j)->GiveLocationName());
							m_RInGame->AddResource("SEALSIGN", TokenPtr);
							break;
						}

					}
				}
				break;
			}
			while (movementPoints > 0)
			{
				system("CLS");
				GlobalCounter2 = 1;
				GlobalCounter = 1;
				DisplayPlayerStats(i);
				cout << "You are in: " << m_RInGame->GiveCharacterPtr(i)->GiveLocationName() << endl;

				cout << GlobalCounter << ". Stay here" << endl;
				GlobalCounter++;
				cout << GlobalCounter << ". Move" << endl;
				GlobalCounter++;
				cout << GlobalCounter << ". Search location" << endl;
				GlobalCounter++;
				

				cout << endl << "Choice: ";
				cin >> choice; 

				GlobalCounter = 1;

				switch(choice)
				{
				case 1:
					movementPoints = 0;
					break;
				case 2:
					system("cls");
					cout << "Movement points: " << movementPoints << endl << endl;
					GlobalCounter2 = 1;
					GlobalCounter = 1;
					if (m_RMap->CheckIfTheresSideLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data())) //If player is in side location
					{
						cout << GlobalCounter << ". Go to: " << m_RMap->GiveStreetAttached(m_RInGame->GiveCharacterPtr(i)->GiveLocationName()) << endl;
						strArray[GlobalCounter] = m_RMap->GiveStreetAttached(m_RInGame->GiveCharacterPtr(i)->GiveLocationName());
						GlobalCounter++;
					}
					if (m_RMap->CheckIfTheresStreet(m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data())) //if player is on street
					{
						for (int j = 0;m_RMap->GiveMainLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName(), j) != "error"; j++)
						{
							cout << GlobalCounter << ". Go to: " << m_RMap->GiveMainLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName(), j) << endl;
							strArray[GlobalCounter] = m_RMap->GiveMainLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName(), j);
							GlobalCounter++;
						}
						for (int j = 0;m_RMap->GiveSideLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName(), j) != "error"; j++)
						{
							cout << GlobalCounter << ". Go to: " << m_RMap->GiveSideLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName(), j) << endl;
							strArray[GlobalCounter] = m_RMap->GiveSideLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName(), j);
							GlobalCounter++;
						}
					}

					cout << GlobalCounter << ". Exit" << endl;

					cout << endl << "your choice: ";
					cin >> choice;
					if (choice == GlobalCounter) break;
					if (choice > 0 && choice < GlobalCounter)
					{
						m_RInGame->GiveCharacterPtr(i)->ChangeLocation(strArray[choice]);
						movementPoints--;
						EventControllerUnit.CheckIfBattle();
						if (EventControllerUnit.CheckIfGateOnLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName()) != false) //gate on this location
						{
							system("cls");
							cout << "Gate pulled you inside " << m_RInGame->GiveGatePtr(m_RInGame->GiveCharacterPtr(i)->GiveLocationName())->GiveDestination() << "!" << endl;
							_getch();
							movementPoints = 0;
							m_RInGame->GiveCharacterPtr(i)->ChangeLocation(m_RInGame->GiveGatePtr(m_RInGame->GiveCharacterPtr(i)->GiveLocationName())->GiveDestination());
							m_RInGame->GiveCharacterPtr(i)->ChangeOLStage(true);
						}
					}
					GlobalCounter2 = 1;
					GlobalCounter = 1;

					break;
				case 3:
					if (m_RMap->CheckIfTheresStreet(m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data()))
					{
						system("cls"); 
						cout << "Nothing to search for here" << endl;
						movementPoints--;
					}
					if (m_RMap->CheckIfTheresSideLocation(m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data()))
					{
						system("cls");
						cout << "After examination you can see that you can gain " << m_RMap->GiveSideLocationResource(0, m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data())
							<< " and " << m_RMap->GiveSideLocationResource(1, m_RInGame->GiveCharacterPtr(i)->GiveLocationName().data()) << " here. " << endl;
						{
							EventPtrRoot = new CMyEvent();
							EventPtr = EventPtrRoot;
							
							{
								EventPtr->AddEventCommnand("GTAP");
								EventPtr->AddEventCommnand("TOKEN");
								EventPtr->AddEventCommnand("CLUE");
								EventPtr->AddEventCommnand("INGAME");
								EventPtr->AddEventCommnand("ALL");

								EventPtr = EventPtr->CreateNextEvent();

								EventPtr->AddEventCommnand("THROWAWAY");
								EventPtr->AddEventCommnand("NAME");
								EventPtr->AddEventCommnand("IFNOT");
								EventPtr->AddEventCommnand(m_RInGame->GiveCharacterPtr(i)->GiveLocationName());

								EventPtr = EventPtr->CreateNextEvent();

								EventPtr->AddEventCommnand("CHANGEATT");
								EventPtr->AddEventCommnand("LOCATION");
								EventPtr->AddEventCommnand(m_RInGame->GiveCharacterPtr(i)->GiveSurname());

								EventPtr = EventPtr->CreateNextEvent();

								EventPtr->AddEventCommnand("CHECKIFEMPTY");

								EventPtr = EventPtr->CreateNextEvent();

								EventPtr->AddEventCommnand("TEXT");
								EventPtr->AddEventCommnand("No clues were find this time");

								EventPtr = EventPtr->GivePrevPtr();
								EventPtr = EventPtr->CreateAdditionalEvent();

								EventPtr->AddEventCommnand("GIVE");
								EventPtr->AddEventCommnand("TOKEN");
								EventPtr->AddEventCommnand("ARRAY");
								EventPtr->AddEventCommnand(m_RInGame->GiveCharacterPtr(i)->GiveSurname());

								EventPtr = EventPtr->CreateNextEvent();

								EventPtr->AddEventCommnand("TEXT");
								EventPtr->AddEventCommnand("You found some clues!");
							}

							EventControllerUnit.CallEvent(*EventPtrRoot);

							
							delete(EventPtrRoot);
							EventPtr = NULL;
							EventPtrRoot = NULL;
						}
						_getch();
						movementPoints--;
					}

					break;
				default:

					break;
				}
				if (movementPoints <= 0) break;
			}//Arkham

		}
	} // MOVEMENT
	return false;
}

bool CGameController::ThirdPhase()
{
	system("cls");

	string choiceStr;
	int choiceInt = 0;
	int successNumber = 0;

	CGate *gatePtr = NULL;

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_RInGame->GiveCharacterPtr(i) == NULL)break;
		//player on location with gate
		for (int j = 0; j < MAXRESOURCENUMBER; j++)
		{
			if (m_RInGame->GiveTokenPtr("SEALSIGN", j) == NULL)break;
			if (m_RInGame->GiveTokenPtr("SEALSIGN", j)->GiveLocationName() == m_RInGame->GiveCharacterPtr(i)->GiveLocationName() && m_RInGame->GiveGatePtr(m_RInGame->GiveCharacterPtr(i)->GiveLocationName())!=NULL)
			{
				system("cls");

				gatePtr = m_RInGame->GiveGatePtr(m_RInGame->GiveCharacterPtr(i)->GiveLocationName());

				cout << "You are standing in front of a gate, ready to close it." << endl
					<< "After examination, portal has " << gatePtr->GiveHP() << " difficulty level." << endl;
				cout << "Do you want to:" << endl
					<< "1. Leave it" << endl
					<< "2. Close it with fighting skill" << endl
					<< "3. Close it with lore skill" << endl;
				cin >> choiceStr;
				choiceInt = atoi(choiceStr.data());

				if (choiceInt == 1) break;
				else if (choiceInt == 2)
				{
					successNumber = EventControllerUnit.CallTest(m_RInGame->GiveCharacterPtr(i), gatePtr->GiveHP(), "FIGHT");
				}
				else if (choiceInt == 3)
				{
					successNumber = EventControllerUnit.CallTest(m_RInGame->GiveCharacterPtr(i), gatePtr->GiveHP(), "LORE");
				}
				else break;
				_getch();

				if (successNumber > 0)
				{
					system("cls");
					cout << "You closed the gate!" << endl;
					gatePtr = m_RInGame->GiveGate(gatePtr);
					m_RBox->AddGate(gatePtr);
					_getch();
				}
				else
				{
					system("cls");
					cout << "You were unsuccessful in closing a gate" << endl;
					_getch();
				}
			}
		}
	}

	return false;
}

bool CGameController::FourthPhase()
{
	return false;
}
