#pragma once
#include "Resource.h"
#include "MyMap.h"

class CEventController
{
private:
	CResource *m_cPack;
	CResource *m_cGame;
	CMyMap    *m_cMap;

	string       m_sArray1[MAXRESOURCENUMBER];
	CGameObject *m_sArray2[MAXRESOURCENUMBER];
public:
	//Accessors
	bool CheckIfObjectArrayEmpty();
	bool CheckIfGateOnLocation(string Location);
	//Modifiers
	inline void SetPack(CResource *Pack) { m_cPack = Pack; };
	inline void SetGame(CResource *Game) { m_cGame = Game; };
	inline void SetMap (CMyMap *Map)     { m_cMap = Map; };

	bool AddToArray       (CGameObject *whatToAdd);
	bool AddToArrayMonster(string dimension, string place);
	//Methods
	bool MoveArrayMonster (string direction);

	bool ClearLocationArray();
	bool ClearObjectArray  ();

	bool RemoveObject(int which);
	bool RemoveObject(string location, bool trigger);

	bool ChangeLocation(string location);

	bool SpawnObject(string whatToSpawn, string whereToSpawn);

	bool CallEvent   (CMyEvent OnCall);
	bool CallMythCard(CCardMyth *OnCall);

	bool DamageCharacter(CCharacter *hero,int number, string whatToDamage);

	bool CheckIfBattle();
	bool CallBattle   (CMonster *monster, CCharacter *hero, bool canDodge = true);

	int CallTest      (CCharacter *hero, int modifier, string type);
	//C/D
	CEventController();
	~CEventController();
};

