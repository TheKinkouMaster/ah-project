#include "MyEvent.h"





bool CMyEvent::CheckNext()
{
	if (m_pNext == NULL) return false;
	return true;
}

bool CMyEvent::CheckAdditional()
{
	if (m_pAdditional == NULL) return false;
	return true;
}

bool CMyEvent::CheckPrev()
{
	if(m_pPrev == NULL)return false;
	return true;
}

bool CMyEvent::AddEventCommnand(string newKeyword)
{
	for (int i = 0; i < 8; i++)
	{
		if (m_sCommand[i] == "empty")
		{
			m_sCommand[i] = newKeyword;
			return true;
		}
	}
	return false;
}

CMyEvent::CMyEvent()
{
	for (int i = 0; i < 8; i++)
	{
		m_sCommand[i] = "empty";
	}
	m_pNext = NULL;
	m_pAdditional = NULL;
	m_pPrev = NULL;
}


CMyEvent::~CMyEvent()
{
}
