#include "EventController.h"
#include <conio.h>



bool CEventController::CheckIfObjectArrayEmpty()
{
	if (m_sArray2[0] == NULL)return true;
	return false;
}

bool CEventController::CheckIfGateOnLocation(string Location)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_cGame->GiveGatePtr(i) == NULL)break;
		if (m_cGame->GiveGatePtr(i)->GiveLocationName() == Location) return true;
	}
	return false;
}

bool CEventController::AddToArray(CGameObject * whatToAdd)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_sArray2[i] == NULL)
		{
			m_sArray2[i] = whatToAdd;
			return true;
		}
	}
	return false;
}

bool CEventController::AddToArrayMonster(string dimension, string place)
{
	CResource *ptr = NULL;

	if (place == "BOX") ptr = m_cPack;
	else ptr = m_cGame;

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (ptr->GiveMonsterPtr(i) == NULL)break;
		if (ptr->GiveMonsterPtr(i)->GiveDimention() == dimension) AddToArray(ptr->GiveMonsterPtr(i));
	}
	return true;
}

bool CEventController::MoveArrayMonster(string direction)
{
	CMonster *ptr;
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_sArray2[i] == NULL)break;
		ptr = static_cast<CMonster*>(m_sArray2[i]);
		if (ptr->GiveMovement() == "Normal")
		{
			ptr->ChangeLocation(m_cMap->GivePath(direction,ptr->GiveLocationName()));
		}
		
	}
	return true;
}

bool CEventController::ClearLocationArray()
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		m_sArray1[i] = "nothing";
	}
	return true;
}

bool CEventController::ClearObjectArray()
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		m_sArray2[i] = nullptr;
	}
	return true;
}

bool CEventController::RemoveObject(int which)
{
	CGameObject *ptr = NULL;

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_sArray2[i] == NULL) return false;
		if (m_sArray2[i + 1] == NULL)
		{
			if (i != which)
			{
				ptr = m_sArray2[i];
				m_sArray2[i] = NULL;
				break;
			}
			else
			{
				ptr = m_sArray2[i];
				break;
			}
		}
	}

	if (ptr != m_sArray2[which])
	{
		m_sArray2[which] = ptr;
	}
	else 
	{
		m_sArray2[which] = NULL;
	}
	return true;
}

bool CEventController::RemoveObject(string location, bool trigger)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_sArray2[i] == NULL)break;
		if (m_sArray2[i]->GiveLocationName() != location)
		{
			RemoveObject(i);
			i--;
		}
	}
	return true;
}

bool CEventController::ChangeLocation(string location)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_sArray2[i] == NULL)break;
		else
		{
			m_sArray2[i]->ChangeLocation(location);
		}
	}
	return true;
}

bool CEventController::SpawnObject(string whatToSpawn, string whereToSpawn)
{
	CGameObject *ptr = NULL;
	if (whatToSpawn == "CLUE")
	{
		ptr = m_cPack->GiveToken("CLUE");
		ptr->ChangeLocation(whereToSpawn);
		m_cGame->AddResource("CLUE", ptr);
	}
	if (whatToSpawn == "GATE")
	{
		ptr = m_cPack->GiveGate();
		ptr->ChangeLocation(whereToSpawn);
		m_cGame->AddGate(static_cast<CGate*>(ptr));
	}
	if (whatToSpawn == "MONSTER")
	{
		ptr = m_cPack->GiveMonster();
		ptr->ChangeLocation(whereToSpawn);
		m_cGame->AddMonster(static_cast<CMonster*>(ptr));
	}
	return true;
}

bool CEventController::CallEvent(CMyEvent OnCall)
{
	CMyEvent *ptr = &OnCall;
	CMonster *monsterPtr = NULL;
	bool ptrMoved = false;

	CResource *resourcePtr = NULL;

	string *commandLine;

	ClearObjectArray();
	ClearLocationArray();
	
	while (ptr != NULL)
	{
		commandLine = ptr->GiveCommandLine();
		if (*commandLine == "GTAL")
		{
			if (*(commandLine + 1) == "SIDELOCATION")
			{
				if (*(commandLine + 2) == "SAFETY")
				{
					if (*(commandLine + 3) == "FALSE")
					{
						m_cMap->GiveSideLocations(false, m_sArray1);
					}
					else
					{
						m_cMap->GiveSideLocations(true, m_sArray1);
					}
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "GTAP")
		{
			if (*(commandLine + 3) == "INGAME") resourcePtr = m_cGame;
			if (*(commandLine + 3) == "INBOX") resourcePtr = m_cPack;

			if (*(commandLine + 1) == "TOKEN")
			{
				for (int i = 0; i < MAXRESOURCENUMBER; i++)
				{
					if(resourcePtr->GiveTokenPtr(*(commandLine + 2),i)==nullptr) break;
					AddToArray(resourcePtr->GiveTokenPtr(*(commandLine + 2), i));
				}
			}

		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "GTAO")
		{
			if (*(commandLine + 4) == "INGAME") resourcePtr = m_cGame;
			if (*(commandLine + 4) == "INBOX") resourcePtr = m_cPack;

			if (*(commandLine + 1) == "MONSTER")
			{
				for (int i = 0; i < MAXRESOURCENUMBER; i++)
				{
					monsterPtr = resourcePtr->GiveMonster(*(commandLine + 3));
					if (monsterPtr == nullptr) break;
					AddToArray(monsterPtr);
				}
			}

		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "MTB")
		{
			for (int i = 0; i < MAXRESOURCENUMBER; i++)
			{
				if (m_sArray2[i] == NULL) break;
				if (m_sArray2[i] != NULL)
				{
					m_cPack->AddMonster(static_cast<CMonster*>(m_sArray2[i]));
					m_sArray2[i] = NULL;
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "SPAWN")
		{
			for (int i = atoi((*(commandLine + 3)).data()); i > 0; i--)
			{
				if (*(commandLine + 2) == "ARRAY")
				{
					for (int j = 0; j < MAXRESOURCENUMBER; j++)
					{
						if (m_sArray1[j] != "nothing")
						{
							SpawnObject(*(commandLine + 1), m_sArray1[j]);
						}
						if (m_sArray1[j] == "nothing")
						{
							break;
						}
					}
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "GIVE")
		{
			if (*(commandLine + 1) == "TOKEN")
			{
				if (*(commandLine + 2) == "ARRAY")
				{
					while (m_sArray2[0] != NULL)
					{
						m_cGame->GiveCharacterPtr(*(commandLine + 3))->AddClueToken(static_cast<CToken*>(m_sArray2[0]));
						RemoveObject(0);
					}
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "THROWAWAY")
		{
			if (*(commandLine + 1) == "NAME")
			{
				if (*(commandLine + 2) == "IFNOT")
				{
					RemoveObject(*(commandLine + 3), true);
				}
				if (*(commandLine + 2) == "IFYES")
				{
					RemoveObject(*(commandLine + 3), false);
				}
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "CHANGEATT")
		{
			if (*(commandLine + 1) == "LOCATION")
			{
				ChangeLocation(*(commandLine + 2));
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "CHECKIFEMPTY")
		{
			if (CheckIfObjectArrayEmpty() == false)
			{
				ptr = ptr->GiveAdditionalPtr();
				ptrMoved = true;
			}
		}
		//////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "TEXT")
		{
			cout << *(commandLine + 1) << endl;
		}
		//////////////////////////////////////////////////////////////////////////////
		if (*commandLine == "STOP")
		{
			_getch();
		}
		//////////////////////////////////////////////////////////////////////////////
		if (ptrMoved == false)
		{
			ptr = ptr->GiveNextPtr();
		}
		ptrMoved = false;
	}

	ClearLocationArray();
	ClearObjectArray();
	return true;
}

bool CEventController::CallMythCard(CCardMyth *OnCall)
{

	bool spawnMonster = true;
	bool multiplicate = false;
	SDimensionType movement;

	//GATE
	if (CheckIfGateOnLocation(OnCall->GiveGateLocation()) == false)
	{
		SpawnObject("GATE", OnCall->GiveGateLocation());
		cout << "Gate appeared at " << OnCall->GiveGateLocation() << endl;
	}
	else multiplicate = true;

	//MONSTER
	if (multiplicate == false && spawnMonster == true) SpawnObject("MONSTER", OnCall->GiveGateLocation());
	if (multiplicate == true && spawnMonster == true);
	if (multiplicate == false && spawnMonster == false);

	//CLUE
	if (CheckIfGateOnLocation(OnCall->GiveClueLocation()) == false)
	{
		cout << "Clue appeared at " << OnCall->GiveClueLocation() << endl;
		SpawnObject("CLUE", OnCall->GiveClueLocation());
	}
	//MOVEMENT
	{
		movement = OnCall->GiveBlackMovement();
		if (movement.circle == true)
		{
			AddToArrayMonster("Circle", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.cross == true)
		{
			AddToArrayMonster("Cross", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.diamond == true)
		{
			AddToArrayMonster("Diamond", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.hexagon == true)
		{
			AddToArrayMonster("Hexagon", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.line == true)
		{
			AddToArrayMonster("Line", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.moon == true)
		{
			AddToArrayMonster("Moon", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.square == true)
		{
			AddToArrayMonster("Square", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.star == true)
		{
			AddToArrayMonster("Star", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		if (movement.triangle == true)
		{
			AddToArrayMonster("Triangle", "INGAME");
			MoveArrayMonster("BLACK");
			ClearObjectArray();
		}
		movement = OnCall->GiveWhiteMovement();
		if (movement.circle == true)
		{
			AddToArrayMonster("Circle", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.cross == true)
		{
			AddToArrayMonster("Cross", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.diamond == true)
		{
			AddToArrayMonster("Diamond", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.hexagon == true)
		{
			AddToArrayMonster("Hexagon", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.line == true)
		{
			AddToArrayMonster("Line", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.moon == true)
		{
			AddToArrayMonster("Moon", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.square == true)
		{
			AddToArrayMonster("Square", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.star == true)
		{
			AddToArrayMonster("Star", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
		if (movement.triangle == true)
		{
			AddToArrayMonster("Triangle", "INGAME");
			MoveArrayMonster("WHITE");
			ClearObjectArray();
		}
	}
	//
	CallEvent(*OnCall->GiveScript());

	return true;
}

bool CEventController::DamageCharacter(CCharacter *hero,int number, string whatToDamage)
{
	CToken *ptr;

	if (whatToDamage == "SANITY")
	{
		for (int i = 0; i < number; i++)
		{
			ptr = hero->RemoveSanityToken();
			if(ptr!=nullptr)m_cPack->AddResource("SANITY", ptr);
			if (ptr == nullptr)return false;
			ptr = NULL;
		}
		return true;
	}

	if (whatToDamage == "HEALTH")
	{
		for (int i = 0; i < number; i++)
		{
			ptr = hero->RemoveHealthToken();
			if(ptr!=nullptr)m_cPack->AddResource("HEALTH", ptr);
			if (ptr == nullptr)return false;
			ptr = NULL;

		}
		return true;
	}

	return true;
}

bool CEventController::CheckIfBattle()
{
	extern bool exit_current_game;
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_cGame->GiveCharacterPtr(i) == NULL)break;
		for (int j = 0; j < MAXRESOURCENUMBER; j++)
		{
			if (m_cGame->GiveMonsterPtr(j) == NULL)break;
			if (m_cGame->GiveMonsterPtr(j)->GiveLocationName() == m_cGame->GiveCharacterPtr(i)->GiveLocationName())
			{
				if (CallBattle(m_cGame->GiveMonsterPtr(j), m_cGame->GiveCharacterPtr(i)) == false)
				{
					exit_current_game = true;
				}
			}
		}
	}
	return false;
}

bool CEventController::CallBattle(CMonster * monster, CCharacter * hero, bool canDodge)
{
	string choiceStr;
	CGameObject *ptr = NULL;
	int choiceInt = 0;

	int successNumber = 0;
	bool effect = false;

	system("cls");

	cout << "You've met a monster called " << monster->GiveName() << endl;

	cout << "Do you want to try to sneak by him?" << endl;
	cin >> choiceStr;
	choiceInt = atoi(choiceStr.data());



	if (choiceInt != 0)//trying to run away
	{
		successNumber = CallTest(hero, monster->GiveAwareness(), "SNEAK");
		_getch();

		if (successNumber > 0)
		{
			cout << "You managed to sneak by enemy!";
			_getch();
			return true;
		}
		else
		{
			effect = DamageCharacter(hero, monster->GivePhysicalDMG(), "HEALTH");
			cout << "You've suffered " << monster->GivePhysicalDMG() << " damage.";
			_getch();
			if (effect == false) return false;
		}
	}

	//Sanity test
	if (monster->GiveSanityDMG() != 0)
	{
		cout << "Because of what you've seen you have pass test of your will!" << endl;
		successNumber = CallTest(hero, monster->GiveSanityMod(),"WILL");
		_getch();
		if (successNumber > 0) cout << "You've passes a trial! " << endl;
		else
		{
			effect = DamageCharacter(hero, monster->GiveSanityDMG(), "SANITY");
			cout << "You've suffered " << monster->GiveSanityDMG() << "  sanity damage.";
			_getch();
			if (effect == false) return false;
		}
		_getch();
		
	}
	//Sanity test end

	
	{ //fight
		while (true)
		{
			system("cls");
			cout << "You are fighting against " << monster->GiveName() << endl;
			cout << "His damage modifier is " << monster->GivePhysicalMod() << endl;
			cout << "His damage is " << monster->GivePhysicalDMG() << endl;
			cout << "His health is " << monster->GiveHealth() << endl;
			cout << "His awareness is " << monster->GiveAwareness() << endl;

			cout << "1. Run away" << endl;
			cout << "2. Fight" << endl;

			cin >> choiceStr;
			choiceInt = atoi(choiceStr.data());

			if (choiceInt == 1)
			{
				successNumber = CallTest(hero, monster->GiveAwareness(), "SNEAK");
				_getch();

				if (successNumber > 0)
				{
					cout << "You managed to sneak by enemy!";
					_getch();
					return true;
				}
				else
				{
					effect = DamageCharacter(hero, monster->GivePhysicalDMG(), "HEALTH");
					cout << "You've suffered " << monster->GivePhysicalDMG() << " damage.";
					_getch();
					if (effect == false) return false;
				}
			}
			else if (choiceInt == 2)
			{
				successNumber = CallTest(hero, monster->GivePhysicalMod(), "FIGHT");
				_getch();

				if (successNumber >= monster->GiveHealth())
				{
					cout << "You managed to take down the enemy!";
					m_cGame->DeleteMonster(monster);
					hero->AddMonsterTrophy(static_cast<CMonster*>(monster));
					_getch();
					return true;
				}
				else
				{
					effect = DamageCharacter(hero, monster->GivePhysicalDMG(), "HEALTH");
					cout << "You've suffered " << monster->GivePhysicalDMG() << " damage.";
					_getch();
					if (effect == false) return false;
				}
			}
		}
	}


	return false;
}

int CEventController::CallTest(CCharacter * hero, int modifier, string type)
{
	int diceNumber = 0;
	int result = 0;

	diceNumber = diceNumber + modifier;

	if (type == "SPEED")diceNumber = diceNumber + hero->GiveSpeed();
	if (type == "SNEAK")diceNumber = diceNumber + hero->GiveSneak();
	if (type == "FIGHT")diceNumber = diceNumber + hero->GiveFight();
	if (type == "WILL")diceNumber = diceNumber + hero->GiveWill();
	if (type == "LORE")diceNumber = diceNumber + hero->GiveLore();
	if (type == "LUCK")diceNumber = diceNumber + hero->GiveLuck();

	if (diceNumber > 0)
	{
		result = roleDice(diceNumber,"NORMAL");
	}
	return result;
}

CEventController::CEventController()
{
	m_cPack = NULL;
	m_cGame = NULL;
	m_cMap = NULL;

	ClearLocationArray();
	ClearObjectArray();
}

CEventController::~CEventController()
{
}
