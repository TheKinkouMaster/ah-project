#include "AdditionalFunctionsAndStructures.h"
#include <iostream>
#include <ctime>

int roleDice(int howManyTimes, string mode = "NORMAL")
{
	int wantedValue = 0;
	int successNumber = 0;
	int tmpInt = 0;

	if (mode == "NORMAL") wantedValue = 5;
	if (mode == "CURSE") wantedValue = 6;
	if (mode == "BLESS") wantedValue = 4;

	srand((unsigned int)time(NULL));

	cout << "You rolled ";
	for (int i = 0; i < howManyTimes; i++)
	{
		tmpInt = ((rand() % 6) + 1);
		if (tmpInt >= wantedValue) successNumber++;
		cout << tmpInt << " " << endl;
	}
	cout << "!" << endl;

	return successNumber;
}
bool exit_current_game = false;