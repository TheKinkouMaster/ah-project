#pragma once
#include "MapObject.h"
class CMapOutterWorld :
	public CMapObject
{
private:
	bool m_bIsRed;
	bool m_bIsBlue;
	bool m_bIsYellow;
	bool m_bIsGreen;
public:
	//Accessors
	inline bool isRed() { return m_bIsRed; };
	inline bool isBlue() { return m_bIsBlue; };
	inline bool isGreen() { return m_bIsGreen; };
	inline bool isYellow() { return m_bIsYellow; };

	//Modifiers
	bool ChangeColor(const char color = 'E', bool wantedValue = false);

	//Methods

	//C/D
	CMapOutterWorld(string name = "default_name", bool red = false, bool blue = false, bool green = false, bool yellow = false);
	~CMapOutterWorld();
};

