#pragma once
#include "Card.h"
class CCardMyth :
	public CCard
{
private:
	CMyEvent m_sScripts;

	string m_sClueLocation;
	string m_sGateLocation;

	SDimensionType m_sWhite;
	SDimensionType m_sBlack;
public:
	//Accessors
	CMyEvent *GiveScript()      { return &m_sScripts; };
	
	string   GiveClueLocation() { return m_sClueLocation; };
	string   GiveGateLocation() { return m_sGateLocation; };

	SDimensionType GiveBlackMovement() { return m_sBlack; };
	SDimensionType GiveWhiteMovement() { return m_sWhite; };

	//Modifiers
	bool ChangeClueLocation(string newLocation);
	bool ChangeGateLocation(string newLocation);
	bool AddWhiteMovement  (string sign);
	bool AddBlackMovement  (string sign);

	bool ChangeScript      (CMyEvent newScript);

	//Methods

	//C/D
	CCardMyth();
	~CCardMyth();
};

