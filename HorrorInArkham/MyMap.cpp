#include "MyMap.h"
#include <fstream>
using namespace std;



int CMyMap::returnPosition(string name)
{
	int parameter = 0;
	for (int i = 0; i < 4; i++)
	{
		switch (i)
		{
		case 0:
			parameter = maxMapLocations;
			break;
		case 1: 
			parameter = maxMapOutterLocations;
			break;
		case 2:
			parameter = maxMapSideLocations;
			break;
		case 3:
			parameter = maxMapStreets;
			break;
		default:
			parameter = 0;
			break;
		}
		for (int j = 0; j < parameter; j++)
		{
			if (i == 0) {
				if (m_CMAPLocations[j] == NULL && i == 0) break;
				if (m_CMAPLocations[j]->GiveName() == name && i == 0) return j;
				if (m_CMAPLocations[j + 1] == NULL && i == 0) break;
			}
			else if (i == 1) {
				if (m_CMAPOutterLocations[j] == NULL && i == 1) break;
				if (m_CMAPOutterLocations[j]->GiveName() == name && i == 1) return j;
				if (m_CMAPOutterLocations[j + 1] == NULL && i == 1) break;
			}
			else if (i == 2) {
				if (m_CMAPSideLocations[j] == NULL && i == 2) break;
				if (m_CMAPSideLocations[j]->GiveName() == name && i == 2) return j;
				if (m_CMAPSideLocations[j + 1] == NULL && i == 2) break;
			}
			else if (i == 3) {
				if (m_CMAPStreets[j] == NULL && i == 3) break;
				if (m_CMAPStreets[j]->GiveName() == name && i == 3) return j;
				if (m_CMAPStreets[j + 1] == NULL && i == 3) break;
			}
			else break;
		}
	}
	return 0;
}

string CMyMap::GiveLocation(int number)
{
	if (m_CMAPLocations[number] == NULL) return "error";
	return m_CMAPLocations[number]->GiveName();
}

string CMyMap::GiveOutterLocation(int number)
{
	if (m_CMAPOutterLocations[number] == NULL) return "error";
	return m_CMAPOutterLocations[number]->GiveName();
}

string CMyMap::GiveSideLocation(int number)
{
	if (m_CMAPSideLocations[number] == NULL) return "error";
	return m_CMAPSideLocations[number]->GiveName();
}

string CMyMap::GiveSideLocation(string streetname, int number)
{
	if (m_CMAPStreets[returnPosition(streetname)]->GiveSideLinks(number).length() < 2 || m_CMAPStreets[returnPosition(streetname)]->GiveSideLinks(number) == "Empty")return "error";
	return m_CMAPStreets[returnPosition(streetname)]->GiveSideLinks(number);
}

bool CMyMap::GiveSideLocation(string streetname, string arraystr[])
{
	int j = 0;
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		if (m_CMAPSideLocations[i] == NULL)break;
		if (m_CMAPSideLocations[i]->GiveStreet() == streetname)
		{
			arraystr[j] = m_CMAPSideLocations[i]->GiveName();
			j++;
		}
	}
	arraystr[j] == "END";
	return true;
}

string CMyMap::GiveMainLocation(string streetname, int number)
{
	if (m_CMAPStreets[returnPosition(streetname.data())]->GiveMainLinks(number).length() < 2 || m_CMAPStreets[returnPosition(streetname.data())]->GiveMainLinks(number) == "Empty")return "error";
	return m_CMAPStreets[returnPosition(streetname.data())]->GiveMainLinks(number);
}

string CMyMap::GiveStreet(int number)
{
	if (m_CMAPStreets[number] == NULL) return "error";
	return m_CMAPStreets[number]->GiveName();
}

string CMyMap::GiveStreetAttached(string name)
{
	string returnedValue = m_CMAPSideLocations[returnPosition(name.data())]->GiveStreet();
	return returnedValue;
}

string CMyMap::GivePath(string direction, string locationName)
{
	if (CheckIfTheresSideLocation(locationName.data()) == true) return m_CMAPSideLocations[returnPosition(locationName)]->GiveStreet();
	if (CheckIfTheresStreet(locationName.data()) == true && direction == "BLACK")return m_CMAPStreets[returnPosition(locationName)]->GiveBlackPath();
	if (CheckIfTheresStreet(locationName.data()) == true && direction == "WHITE")return m_CMAPStreets[returnPosition(locationName)]->GiveWhitePath();

	return locationName;
}

bool CMyMap::GiveSideLocations(bool Safety, string arraystr[])
{
	int j = 0;
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		if (m_CMAPSideLocations[i] == NULL)break;
		if (m_CMAPSideLocations[i]->GiveIsSafe() == Safety)
		{
			arraystr[j] = m_CMAPSideLocations[i]->GiveName();
			j++;
		}
	}
	return true;
}

string CMyMap::GiveSideLocationResource(int which, string locationName)
{
	int result = m_CMAPSideLocations[returnPosition(locationName)]->GiveResourceTrait(which);

	switch (result)
	{
	case 1:
		return "Cash";
		break;
	case 2:
		return "Clue";
		break;
	case 3:
		return "Common Item";
		break;
	case 4:
		return "Rare Item";
		break;
	case 5:
		return "Magic Item";
		break;
	case 6:
		return "Ability";
		break;
	case 7:
		return "Ally";
		break;
	case 8:
		return "Bless";
		break;
	case 9:
		return "Sanity";
		break;
	case 10:
		return "Health";
		break;
	default:

		return "error";
		break;
	}

	return "error";
}

bool CMyMap::CheckIfTheresStreet(string name)
{
	for (int i = 0; m_CMAPStreets[i] != NULL; i++)
	{
		if(m_CMAPStreets[i]->GiveName() == name)return true;
		if ((i+1) == maxMapStreets) return false;
	}
	return false;
}

bool CMyMap::CheckIfTheresSideLocation(string name)
{
	for (int i = 0; m_CMAPSideLocations[i] != NULL; i++)
	{
		if (m_CMAPSideLocations[i]->GiveName() == name)return true;
		if ((i + 1) == maxMapSideLocations) return false;
	}
	return false;
}

bool CMyMap::CheckIfTheresLocation(string name)
{
	for (int i = 0;m_CMAPLocations[i] != NULL; i++)
	{
		if (m_CMAPLocations[i]->GiveName() == name)return true;
		if ((i + 1) == maxMapLocations) return false;
	}
	return false;
}

bool CMyMap::CheckIfTheresOutterWorld(string name)
{
	for (int i = 0; m_CMAPOutterLocations[i] != NULL; i++)
	{
		if (m_CMAPOutterLocations[i]->GiveName() == name)return true;
		if ((i + 1) == maxMapOutterLocations) return false;
	}
	return false;
}

bool CMyMap::AddNewStreet(string streetName)
{
	if(CheckIfTheresStreet(streetName)==true)return false;
	for (int i = 0; i < maxMapStreets; i++)
	{
		if (m_CMAPStreets[i] == NULL)
		{
			m_CMAPStreets[i] = new CMapStreet(streetName);
			return true;
		}
	}
	return false;
}

bool CMyMap::AddNewSideLocation(string SideLocationName)
{
	if (CheckIfTheresSideLocation(SideLocationName) == false)
	{
		for (int i = 0; i < maxMapSideLocations; i++)
		{
			if (m_CMAPSideLocations[i] == NULL)
			{
				m_CMAPSideLocations[i] = new CMapSideLocation(SideLocationName);
				return true;
			}
		}
	}
	return false;
}

bool CMyMap::AddNewOutterLocation(string OutterLocationName)
{
	for (int i = 0; i < maxMapOutterLocations; i++)
	{
		if (CheckIfTheresOutterWorld(OutterLocationName) == true) break;
		if (m_CMAPOutterLocations[i] == NULL)
		{
			m_CMAPOutterLocations[i] = new CMapOutterWorld(OutterLocationName);
			return true;
		}
	}
	return false;
}

bool CMyMap::AddnewLocation(string LocationName)
{
	for (int i = 0; i < maxMapLocations; i++)
	{
		if (CheckIfTheresLocation(LocationName)) break;
		if (m_CMAPLocations[i] == NULL)
		{
			m_CMAPLocations[i] = new CMapLocation(LocationName);
			return true;
		}
	}
	return false;
}

bool CMyMap::AddNewMainLink(string targetStreet, string linkName)
{
	if (CheckIfTheresStreet(targetStreet)==true && CheckIfTheresStreet(linkName)==true)
	{
		m_CMAPStreets[returnPosition(targetStreet)]->AddNewMainLink(linkName);
		m_CMAPStreets[returnPosition(linkName)]->AddNewMainLink(targetStreet);
		return true;
	}
	return false;
}

bool CMyMap::AddNewSideLink(string targetStreet, string linkName)
{
	if (CheckIfTheresStreet(targetStreet) == true && CheckIfTheresSideLocation(linkName) == true)
	{
		m_CMAPStreets[returnPosition(targetStreet)]->AddNewSideLink(linkName);
		AddNewSideLocation(linkName);
		m_CMAPSideLocations[returnPosition(linkName)]->ChangeStreet(targetStreet);
		return true;
	}
	return false;
}

bool CMyMap::ChangeBlackPath(string targetStreet, string linkName)
{
	if (CheckIfTheresStreet(targetStreet))
	{
		for (int i = 0; i < maxMapStreets; i++)
		{
			if (m_CMAPStreets[i]->GiveName() == targetStreet)
			{
				m_CMAPStreets[i]->ChangeBlackPath(linkName);
				return true;
			}
			if (m_CMAPStreets[i + 1] == NULL) return false;
		}
	}
	return false;
}

bool CMyMap::ChangeWhitePath(string targetStreet, string linkName)
{
	if (CheckIfTheresStreet(targetStreet))
	{
		for (int i = 0; i < maxMapStreets; i++)
		{
			if (m_CMAPStreets[i]->GiveName() == targetStreet)
			{
				m_CMAPStreets[i]->ChangeWhitePath(linkName);
				return true;
			}
			if (m_CMAPStreets[i + 1] == NULL) return false;
		}
	}
	return false;
}

bool CMyMap::ConnectSideLocation(string targetLocation, string streetName)
{
	if (CheckIfTheresSideLocation(targetLocation) == 1 && CheckIfTheresStreet(streetName) == 1)
	{
		m_CMAPSideLocations[returnPosition(targetLocation)]->ChangeStreet(streetName);
		m_CMAPStreets[returnPosition(streetName)]->AddNewSideLink(targetLocation);
	}
	return false;
}

bool CMyMap::ChangeSideLocationsResources(string targetLocation, int res1, int res2, bool cer1, bool cer2)
{
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		if (m_CMAPSideLocations[i]->GiveName() == targetLocation)
		{
			m_CMAPSideLocations[i]->ChangeResourceTrait(0, res1, cer1);
			m_CMAPSideLocations[i]->ChangeResourceTrait(1, res2, cer2);
			return true;
		}
		if (m_CMAPSideLocations[i + 1] == NULL)return false;
	}
	return false;
}

bool CMyMap::ChangeSideLocationSafety(string targetLocation, bool safety)
{
	m_CMAPSideLocations[returnPosition(targetLocation)]->ChangeSafety(safety);
	return true;
}

bool CMyMap::ModifyColorOfOutterWorld(string targetWorld, const char color, bool state)
{
	m_CMAPOutterLocations[returnPosition(targetWorld)]->ChangeColor(color, state);
	return true;
}

bool CMyMap::OpenMapFromFile(string filename)
{
	fstream file;
	string command = "nothing";
	string locationName = "nothing";
	string locationName2 = "nothing";

	bool btmp1;
	bool btmp2;
	bool btmp3;
	bool btmp4;

	int itmp1;
	int itmp2;

	file.open(filename, ios::in);

	if (file.good() == false)
	{
		return false;
	}

	while (true)
	{
		file >> command;
		if (command == "START")break;
		if (file.eof() == true) return false;
	}
	while (command != "END")
	{
		file >> command;
		if (command == "STREETS")
		{
			while (command != "OUT")
			{
				file >> command;
				////////////////////////////////////////////////////////////////////////////////
				if (command == "STREET")
				{
					locationName.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if(locationName.length()!=0)locationName.append(" ");
							locationName.append(command);
						}
					}
					AddNewStreet(locationName.data());
				}
				////////////////////////////////////////////////////////////////////////////////
				if (command == "STREETLINKER")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName2.length() != 0)locationName2.append(" ");
							locationName2.append(command);
						}
					}
					AddNewSideLink(locationName.data(), locationName2.data());
				}
				////////////////////////////////////////////////////////////////////////////////
				if (command == "STREETLINK")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName2.length() != 0)locationName2.append(" ");
							locationName2.append(command);
						}
					}
					AddNewMainLink(locationName.data(), locationName2.data());
				}
				////////////////////////////////////////////////////////////////////////////////
				if (command == "PATHMARKERWHITE")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName2.length() != 0)locationName2.append(" ");
							locationName2.append(command);
						}
					}
						ChangeWhitePath(locationName.data(), locationName2.data());
				}
				////////////////////////////////////////////////////////////////////////////////
				if (command == "PATHMARKERBLACK")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName2.length() != 0)locationName2.append(" ");
							locationName2.append(command);
						}
					}
					ChangeBlackPath(locationName.data(), locationName2.data());
				}
			}
		}
		if (command == "SIDELOCATIONS")
		{
			while (command != "OUT")
			{
				file >> command;
				if (command == "LOCATION")
				{
					locationName.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					AddNewSideLocation(locationName.data());
				}
				if (command == "LOCATIONSTREET")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName2.length() != 0)locationName2.append(" ");
							locationName2.append(command);
						}
					}
					m_CMAPSideLocations[returnPosition(locationName.data())]->ChangeStreet((locationName2).data());
					m_CMAPStreets[returnPosition((locationName2).data())]->AddNewSideLink((locationName).data());
				}
				if (command == "LOCATIONSAFETY")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					file >> btmp1;
					m_CMAPSideLocations[returnPosition(locationName.data())]->ChangeSafety(btmp1);
				}
				if (command == "LOCATIONRESOURCE")
				{
					locationName.clear();
					while (command != "TO")
					{
						file >> command;
						if (command != "TO")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					file >> itmp1;
					file >> btmp1;
					file >> itmp2;
					file >> btmp2;
					m_CMAPSideLocations[returnPosition(locationName.data())]->ChangeResourceTrait(0, itmp1, btmp1);
					m_CMAPSideLocations[returnPosition(locationName.data())]->ChangeResourceTrait(1, itmp2, btmp2);
				}
			}
		}
		if (command == "LOCATIONS")
		{
			while (command != "OUT")
			{
				file >> command;
				locationName.clear();
				locationName2.clear();



				///////////////////////////////////////////////////////
				if (command == "LOCATION")
				{
					locationName.clear();
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					AddnewLocation(locationName.data());
				}
				///////////////////////////////////////////////////////
			}
		}
		if (command == "OUTTERWORLDS")
		{
			while (command != "OUT")
			{
				file >> command;
				locationName.clear();
				locationName2.clear();

				

				///////////////////////////////////////////////////////
				if (command == "OUTTERWORLD")
				{
					locationName.clear();
					locationName2.clear();
					while (command != "EOL")
					{
						file >> command;
						if (command != "EOL")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					AddNewOutterLocation(locationName.data());
				}
				///////////////////////////////////////////////////////
				if (command == "OUTTERWORLDCOLOR")
				{
					locationName.clear();
					while (command != "WITH")
					{
						file >> command;
						if (command != "WITH")
						{
							if (locationName.length() != 0)locationName.append(" ");
							locationName.append(command);
						}
					}
					file >> btmp1 >> btmp2 >> btmp3 >> btmp4;
					m_CMAPOutterLocations[returnPosition(locationName.data())]->ChangeColor('B', btmp1);
					m_CMAPOutterLocations[returnPosition(locationName.data())]->ChangeColor('G', btmp2);
					m_CMAPOutterLocations[returnPosition(locationName.data())]->ChangeColor('R', btmp3);
					m_CMAPOutterLocations[returnPosition(locationName.data())]->ChangeColor('Y', btmp4);
				}
				/////////////////////////////////////////////////////////
			}
		}
	}

	file.close();
	return true;
}

bool CMyMap::SaveMapToFIle(string  filename)
{
	fstream file;

	file.open(filename, ios::out);

	file << "START\n";

	file << "STREETS\n";
	for (int i = 0; i < maxMapStreets; i++)
	{
		if (m_CMAPStreets[i] == NULL)break;
		file << "STREET " << m_CMAPStreets[i]->GiveName() << " EOL\n";
	}
	file << "\n\n";


	for (int i = 0; i < maxMapStreets; i++)
	{
		if (m_CMAPStreets[i] == NULL)break;
		for (int j = 0; j < maxMainLinkNumber; j++)
		{
			if (m_CMAPStreets[i]->GiveMainLinks(j)!= "Empty")file << "STREETLINK " << m_CMAPStreets[i]->GiveName() << " TO " << m_CMAPStreets[i]->GiveMainLinks(j) << " EOL\n";
		}
		if (m_CMAPStreets[i]->GiveBlackPath() != "NONE") file << "PATHMARKERBLACK " << m_CMAPStreets[i]->GiveName() << " TO " << m_CMAPStreets[i]->GiveBlackPath() << " EOL\n";
		if (m_CMAPStreets[i]->GiveWhitePath() != "NONE") file << "PATHMARKERWHITE " << m_CMAPStreets[i]->GiveName() << " TO " << m_CMAPStreets[i]->GiveWhitePath() << " EOL\n";
	}

	for (int i = 0; i < maxMapStreets; i++)
	{
		if (m_CMAPStreets[i] == NULL)break;
		for (int j = 0; j < maxSideLinkNumber; j++)
		{
			if (m_CMAPStreets[i]->GiveSideLinks(j) != "Empty")file << "STREETLINKER " << m_CMAPStreets[i]->GiveName() << " TO " << m_CMAPStreets[i]->GiveSideLinks(j) << " EOL\n";
		}
	}
	file << "OUT\n";
	file << "SIDELOCATIONS\n";

	for (int i = 0; i < maxMapSideLocations; i++)
	{
		if (m_CMAPSideLocations[i] == NULL)break;
		file << "LOCATION " << m_CMAPSideLocations[i]->GiveName() << " EOL\n";
		file << "LOCATIONSTREET " << m_CMAPSideLocations[i]->GiveName() << " TO " << m_CMAPSideLocations[i]->GiveStreet() << " EOL\n";
		file << "LOCATIONSAFETY " << m_CMAPSideLocations[i]->GiveName() << " TO " << m_CMAPSideLocations[i]->GiveIsSafe() << " EOL\n";
		file << "LOCATIONRESOURCE " << m_CMAPSideLocations[i]->GiveName() << " TO " << m_CMAPSideLocations[i]->GiveResourceTrait(0) << " "
			<< m_CMAPSideLocations[i]->GiveIsCertain(0) << " " << m_CMAPSideLocations[i]->GiveResourceTrait(1) << " " << m_CMAPSideLocations[i]->GiveIsCertain(1) << " EOL\n";;
	}

	file << "OUT\n";
	file << "LOCATIONS\n";
	for (int i = 0; i < maxMapLocations; i++)
	{
		if (m_CMAPLocations[i] == NULL)break;
		file << "LOCATION " << m_CMAPLocations[i]->GiveName() << " EOL\n";
	}
	file << "OUT\n";
	file << "OUTTERWORLDS\n";
	for (int i = 0; i < maxMapOutterLocations; i++)
	{
		if (m_CMAPOutterLocations[i] == NULL)break;
		file << "OUTTERWORLD " << m_CMAPOutterLocations[i]->GiveName() << " EOL\n";
		file << "OUTTERWORLDCOLOR " << m_CMAPOutterLocations[i]->GiveName() << " WITH " << m_CMAPOutterLocations[i]->isBlue() << " " << m_CMAPOutterLocations[i]->isGreen() << " "
			<< m_CMAPOutterLocations[i]->isRed() << " " << m_CMAPOutterLocations[i]->isYellow() << " EOL\n";
	}
	file << "OUT\n";
	file << "END";

	return false;
}

CMyMap::CMyMap()
{
	for (int i = 0; i < maxMapLocations; i++)
	{
		m_CMAPLocations[i] = NULL;
	}
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		m_CMAPSideLocations[i] = NULL;
	}
	for (int i = 0; i < maxMapOutterLocations; i++)
	{
		m_CMAPOutterLocations[i] = NULL;
	}
	for (int i = 0; i < maxMapStreets; i++)
	{
		m_CMAPStreets[i] = NULL;
	}
	for (int i = 0; i < 11; i++)
	{
		panicTrait[i] = NULL;
	}
}

CMyMap::~CMyMap()
{
	for (int i = 0; i < maxMapLocations; i++)
	{
		if(m_CMAPLocations[i] != NULL) delete(m_CMAPLocations[i]);
	}
	for (int i = 0; i < maxMapSideLocations; i++)
	{
		if (m_CMAPSideLocations[i] != NULL) delete(m_CMAPSideLocations[i]);
	}
	for (int i = 0; i < maxMapOutterLocations; i++)
	{
		if (m_CMAPOutterLocations[i] != NULL) delete(m_CMAPOutterLocations[i]);
	}
	for (int i = 0; i < maxMapStreets; i++)
	{
		if (m_CMAPStreets[i] != NULL) delete(m_CMAPStreets[i]);
	}
}
