#include "Editor.h"



void CEditor::mainEditorLoop()
{
	string stmp;
	int itmp = 0;
	while (itmp != 10)
	{
		system("cls");
		cout << "1. Map Editor"    << endl;
		cout << "2. Object Editor" << endl;
		cout << "3. End"           << endl;

		cin >> stmp;
		itmp = atoi(stmp.data());

		switch (itmp)
		{
		case 1:
			MapEditorUnit.MainEditorLoop();
			break;
		case 2:
			ObjEditorUnit.MainEditorLoop();
			break;
		case 3:
			itmp = 10;
			break;
		default:
			break;
		}
	}
}

CEditor::CEditor()
{
}


CEditor::~CEditor()
{
}
