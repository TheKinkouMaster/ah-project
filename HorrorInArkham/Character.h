#pragma once
#include "GameObject.h"
#include "Token.h"
#include "Monster.h"
class CCharacter
	: public CGameObject
{
private:
	//Name and info
	string    m_sSurname;
	string    m_sProfession;
	//Name and info End

	//Inventory
	CToken   *m_rHealth[20];
	CToken   *m_rSanity[20];
	CToken   *m_rClues[20];
	CMonster *m_rMonsterTrophies[20];
	//Inventory End

	//Stats
	int m_iMaxHP;
	int m_iMaxSanity;
	int m_iFocus;

	int m_iSpeed;
	int m_iSneak;

	int m_iFight;
	int m_iWill;

	int m_iLore;
	int m_iLuck;

	int m_iFirstIndicator;
	int m_iSecondIndicator;
	int m_iThirdIndicator;
	//Stats End

	//Other
	string m_sStartingPosition;

	bool m_bIsOnFirstOL;
	//Other end
public:
	//Accessors
	inline bool GiveIsOnFirstOL() { return m_bIsOnFirstOL; };
	inline int  GiveSpeed()       { return (m_iSpeed + m_iFirstIndicator); };
	inline int  GiveSneak()       { return (m_iSneak - m_iFirstIndicator); };
	inline int  GiveFight()       { return (m_iFight + m_iSecondIndicator); };
	inline int  GiveWill()        { return (m_iWill - m_iSecondIndicator); };
	inline int  GiveLore()        { return (m_iLore + m_iThirdIndicator); };
	inline int  GiveLuck()        { return (m_iLuck - m_iThirdIndicator); };
	inline int  GiveMaxHP()       { return m_iMaxHP; };
	inline int  GiveMaxSanity()   { return m_iMaxSanity; };

	inline string GiveStartingPosition() { return m_sStartingPosition; };
	inline string GiveSurname()          { return m_sSurname; };
	inline string GiveProfession()       { return m_sProfession; };

	inline int GiveFocus() { return m_iFocus; };

	int GiveHp();
	int GiveSanity();
	int GiveClues();
	int GiveMonsters();
	//Modifiers
	bool MoveIndicator(int which, bool toWhatSide);
	void ChangeOLStage(bool value) { m_bIsOnFirstOL = value; };

	void SetFirstStats  (int speed, int sneak);
	void SetSecondsStats(int fight, int will);
	void SetThirdStats  (int lore, int luck);
	void SetFocus       (int focus) { m_iFocus = focus; };

	void ChangeStartingPosition(string newStartingPosition) { m_sStartingPosition = newStartingPosition; };
	void ChangeSurname         (string newSurname)          { m_sSurname = newSurname; };
	void ChangeProfession      (string newProfession)       { m_sProfession = newProfession; };

	void AddHealthToken(CToken *newToken);
	void AddSanityToken(CToken *newToken);
	void AddClueToken  (CToken *newToken);

	void SetMaxHP    (int newValue) { m_iMaxHP = newValue; };
	void SetMaxSanity(int newValue) { m_iMaxSanity = newValue; };

	CToken * RemoveHealthToken();
	CToken * RemoveSanityToken();
	CToken * RemoveClueToken  ();

	bool AddMonsterTrophy     (CMonster *ptr);
	//Methods

	//C/D
	CCharacter();
	~CCharacter();
};

