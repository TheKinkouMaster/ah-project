#include "Resource.h"
#include <time.h>





bool CResource::SwapObjects(CGameObject * first, CGameObject * second)
{
	CGameObject tmp = *first;

	*first = *second;

	*second = tmp;

	return true;
}

CMonster * CResource::GiveMonsterPtr(int which)
{
	return m_pMonsterArray[which];
}

CMonster * CResource::GiveMonster()
{
	CMonster *ptr = m_pMonsterArray[0];

	if (ptr == NULL) return ptr;
	
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i + 1] != NULL) m_pMonsterArray[i] = m_pMonsterArray[i + 1];
		else
		{
			m_pMonsterArray[i] = ptr;
			break;
		}
	}
	return ptr;
}

CMonster * CResource::GiveMonster(string location)
{
	CMonster *ptrLast = NULL;
	CMonster *ptr = NULL;
	int counter = 0;
	
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i] == NULL)break;
		if (m_pMonsterArray[i + 1] == NULL)
		{
			ptrLast = m_pMonsterArray[i];
			if (ptrLast->GiveLocationName() == location)
			{
				m_pMonsterArray[i] = NULL;
				return ptrLast;
			}
			counter = i;
			break;
		}
	}
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i] == NULL) break;
		if (m_pMonsterArray[i]->GiveLocationName() == location)
		{
			ptr = m_pMonsterArray[i];
			m_pMonsterArray[i] = ptrLast;
			m_pMonsterArray[counter] = NULL;
			return ptr;
		}
	}

	return nullptr;
}


CGate * CResource::GiveGate()
{
	CGate *ptr = NULL;
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pGateArray[i] == NULL)return nullptr;
		else if (m_pGateArray[i + 1] == NULL)
		{
			ptr = m_pGateArray[i];
			m_pGateArray[i] = NULL;
			return ptr;
		}
	}
	return nullptr;
}

CGate * CResource::GiveGate(CGate * ptrToGate)
{
	CGate *lastPtr = NULL;
	CGate *returnValue = nullptr;

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pGateArray[i] == NULL)break;
		if (m_pGateArray[i + 1] == NULL)
		{
			if (m_pGateArray[i] == ptrToGate)
			{
				returnValue = m_pGateArray[i];
				m_pGateArray[i] = NULL;
				return returnValue;
			}
			else
			{
				lastPtr = m_pGateArray[i];
				m_pGateArray[i] = NULL;
				break;
			}
		}
	}

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pGateArray[i] == NULL)break;
		else
		{
			if (m_pGateArray[i] == ptrToGate)
			{
				returnValue = m_pGateArray[i];
				m_pGateArray[i] = lastPtr;
				return returnValue;
			}
		}
	}
	return returnValue;
}

CGate * CResource::GiveGatePtr(int which)
{
	if (m_pGateArray[which] != NULL)return m_pGateArray[which];
	return nullptr;
}

CGate * CResource::GiveGatePtr(string location)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pGateArray[i] == NULL)break;
		if (m_pGateArray[i]->GiveLocationName() == location) return m_pGateArray[i];
	}
	return nullptr;
}

CToken * CResource::GiveToken(string command)
{
	CToken *ptr = nullptr;
	if (command == "CLUE")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (i == 0 && m_RClues[i] == NULL)
				break;
			if (m_RClues[i] == NULL)
			{
				ptr = m_RClues[i - 1];
				m_RClues[i - 1] = NULL;
				break;
			}
		}
	}
	if (command == "HEALTH")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (i == 0 && m_RHealth[i] == NULL)
				break;
			if (m_RHealth[i] == NULL)
			{
				ptr = m_RHealth[i - 1];
				m_RHealth[i - 1] = NULL;
				break;
			}
		}
	}
	if (command == "SANITY")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (i == 0 && m_RSanity[i] == NULL)
				break;
			if (m_RSanity[i] == NULL)
			{
				ptr = m_RSanity[i - 1];
				m_RSanity[i - 1] = NULL;
				break;
			}
		}
	}
	if (command == "SEALSIGN")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (i == 0 && m_RSealSign[i] == NULL)
				break;
			if (m_RSealSign[i] == NULL)
			{
				ptr = m_RSealSign[i - 1];
				m_RSealSign[i - 1] = NULL;
				break;
			}
		}
	}

	return ptr;
}

CToken * CResource::GiveTokenPtr(string command, int which)
{
	if (command == "CLUE" && m_RClues[which]!=nullptr)
	{
		return m_RClues[which];
	}
	if (command == "SEALSIGN" && m_RSealSign[which] != nullptr)
	{
		return m_RSealSign[which];
	}
	return nullptr;
}

CCardMyth * CResource::GiveMythCard()
{
	CCardMyth *ptr = m_pMythArray[0];
	SwapCard();
	return ptr;  
}


CCharacter * CResource::GiveCharacter(int which)
{
	CCharacter *ptr = NULL;
	if (m_RPlayers[0] == NULL)return nullptr;
	if (which == -1)
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
			if (m_RPlayers[i + 1] == NULL)
			{
				ptr = m_RPlayers[i];
				m_RPlayers[i] = NULL;
			}
	}
	else
	{
		if (m_RPlayers[which] == NULL)return nullptr;
		else
		{
			ptr = m_RPlayers[which];
			for (int i = which; i < MAXRESOURCENUMBER; i++)
			{
				if (m_RPlayers[i] == NULL)
				{
					m_RPlayers[which] = m_RPlayers[i];
					break;
				}
			}
		}
	}
	return ptr;
}

CCharacter * CResource::GiveCharacterPtr(int which)
{
	if (m_RPlayers[0] == NULL)return nullptr;
	if (which == -1)
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
			if (m_RPlayers[i + 1] == NULL)return m_RPlayers[i];
	}
	else
	{
		if (m_RPlayers[which] == NULL)return nullptr;
		else
		{
			return m_RPlayers[which];
		}
	}
	return nullptr;
}

CCharacter * CResource::GiveCharacterPtr(string surname)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_RPlayers[i] == NULL)break;
		if (m_RPlayers[i]->GiveSurname() == surname) return m_RPlayers[i];
	}
	return nullptr;
}

bool CResource::AddCard(CCardMyth * card)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMythArray[i] == NULL)
		{
			m_pMythArray[i] = card;
			return true;
			break;
		}
	}
	return false;
}

bool CResource::AddMonster(CMonster *monster)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i] == NULL)
		{
			m_pMonsterArray[i] =  monster;
			return true;
			break;
		}
	}
	return false;
}

bool CResource::AddGate(CGate *gate)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pGateArray[i] == NULL)
		{
			m_pGateArray[i] = gate;
			return true;
			break;
		}
	}
	return false;
}

bool CResource::AddResource(int number, string name)
{
	if (name == "CLUE")
		for (int i = 0; i < number; i++)
			m_RClues[i] = new CToken("Clue");
	if (name == "MONEY")
		for (int i = 0; i < number; i++)
			m_RMoney[i] = new CToken("Money");
	if (name == "HEALTH")
		for (int i = 0; i < number; i++)
			m_RHealth[i] = new CToken("Health");
	if (name == "SANITY")
		for (int i = 0; i < number; i++)
			m_RSanity[i] = new CToken("Sanity");
	if (name == "ELDERGOD")
		for (int i = 0; i < number; i++)
			m_RElderGod[i] = new CToken("Eldergod");
	if (name == "SEALSIGN")
		for (int i = 0; i < number; i++)
			m_RSealSign[i] = new CToken("Sealsign");
	return true;
}

bool CResource::AddResource(string name, CGameObject * ptrToResource)
{
	if (name == "CLUE")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (m_RClues[i] == NULL)
			{
				m_RClues[i] = static_cast<CToken*>(ptrToResource);
				break;
			}
		}
	}
	if (name == "SANITY")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (m_RSanity[i] == NULL)
			{
				m_RSanity[i] = static_cast<CToken*>(ptrToResource);
				break;
			}
		}
	}
	if (name == "HEALTH")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (m_RHealth[i] == NULL)
			{
				m_RHealth[i] = static_cast<CToken*>(ptrToResource);
				break;
			}
		}
	}
	if (name == "SEALSIGN")
	{
		for (int i = 0; i < MAXRESOURCENUMBER; i++)
		{
			if (m_RSealSign[i] == NULL)
			{
				m_RSealSign[i] = static_cast<CToken*>(ptrToResource);
				break;
			}
		}
	}

	return true;
}

bool CResource::AddCharacter(CCharacter * character)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_RPlayers[i] == NULL)
		{
			m_RPlayers[i] = character;
			m_RPlayers[i]->ChangeLocation(m_RPlayers[i]->GiveStartingPosition());
			return true;
			break;
		}
	}
	return false;
}

bool CResource::DeleteMonster(CMonster *ptr)
{
	CMonster *lastptr = NULL;

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i] == NULL) return false;
		if (m_pMonsterArray[i + 1] == NULL)
		{
			lastptr = m_pMonsterArray[i];
			if (lastptr = ptr)
			{
				m_pMonsterArray[i] = NULL;
				return true;
			}
			break;
		}
	}

	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i] == ptr)
		{
			m_pMonsterArray[i] = lastptr;
			return true;
		}
	}
	return false;
}

bool CResource::SwapCard()
{
	CCard *ptr = NULL;

	ptr = m_pMythArray[0];
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMythArray[i + 1] == NULL)
		{
			m_pMythArray[i] = static_cast<CCardMyth*>(ptr);
			break;
		}
		else m_pMythArray[i] = m_pMythArray[i + 1];
	}

	return true;
}

int CResource::returnPosition(const char * name)
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		if (m_pMonsterArray[i] != NULL)
			if (m_pMonsterArray[i]->GiveName() == name)return i;
	}
	return 666;
}

bool CResource::ImportData(const char * filename)
{
	fstream file;
	string command;
	string tmpString;

	CMonster *monsterPtr = NULL;
	CGate *gatePtr = NULL;
	CCharacter *characterPtr = NULL;
	CCardMyth *CardPtr = NULL;
	CMyEvent *EventPtr = NULL;
	CMyEvent *EventPtrRoot = NULL;
	int number = 0;

	file.open(filename, ios::in);
	if (!file.good())return false;

	while (!file.eof())
	{
		file >> command;
		if (command == "MONSTER")
		{
			monsterPtr = new CMonster;
			while (command != "END")
			{
				if (command == "NUMBER")
				{
					file >> command;
					number = atoi(command.data());
					file >> command;
				}
				if (command == "NAME")
				{
					file >> command;
					tmpString.clear();
					while (tmpString != "EOL")
					{
						file >> tmpString;
						if (tmpString != "EOL")
						{
							command.append(" ");
							command.append(tmpString);
						}
					}
					monsterPtr->ChangeName(command);
				}
				if (command == "MOVEMENT")
				{
					file >> command;
					monsterPtr->ChangeMovement(command.data());
					file >> command;
				}
				if (command == "AWERENESS")
				{
					file >> command;
					monsterPtr->ChangeAwereness(atoi(command.data()));
					file >> command;
				}
				if (command == "DIMENSION")
				{
					file >> command;
					monsterPtr->ChangeDimension(command.data());
					file >> command;
				}
				if (command == "HP")
				{
					file >> command;
					monsterPtr->ChangeHealth(atoi(command.data()));
					file >> command;
				}
				if (command == "SANMOD")
				{
					file >> command;
					file >> tmpString;
					file >> tmpString;
					file >> tmpString;
					monsterPtr->ChangeSanity(atoi(command.data()), atoi(tmpString.data()));
					file >> command;
				}
				if (command == "PHYMOD")
				{
					file >> command;
					file >> tmpString;
					file >> tmpString;
					file >> tmpString;
					monsterPtr->ChangePhysical(atoi(command.data()), atoi(tmpString.data()));
					file >> command;
				}
				while (command == "ABILITY")
				{
					if (command == "ABILITY")
					{
						file >> command;
						file >> tmpString;
						file >> tmpString;
						monsterPtr->ChangeAbilities(command.data(), atoi(tmpString.data()));
						file >> command;
					}
					file >> command;
				}
				if (command == "DESCRIPTION")
				{
					file >> command;
					while (true)
					{
						file >> tmpString;
						if (tmpString == "EOL")break;
						else
						{
							command.append(" ");
							command.append(tmpString);
						}
					}
					monsterPtr->ChangeDescription(command);
				}

				file >> command;
			}
			for (int i = 0; i < number; i++)
			{
				AddMonster(monsterPtr);
			}
			monsterPtr = NULL;
		}
		if (command == "TOKEN")
		{
			while (command != "END")
			{
				file >> command;
				if (command == "MONEY")tmpString = "MONEY";
				if (command == "CLUE")tmpString = "CLUE";
				if (command == "SANITY")tmpString = "SANITY";
				if (command == "HEALTH")tmpString = "HEALTH";
				if (command == "ELDERGOD")tmpString = "ELDERGOD";
				if (command == "SEALSIGN")tmpString = "SEALSIGN";
				if (command == "NUMBER")
				{
					file >> command;
					number = atoi(command.data());
				}
			}
			AddResource(number, tmpString);
		}
		if (command == "Gate")
		{
			gatePtr = new CGate();
			while (command != "END")
			{
				if (command == "DESTINATION")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					gatePtr->ChangeDestination(command);
				}
				if (command == "Hard")
				{
					file >> command;
					file >> command;
					gatePtr->ChangeHP(atoi(command.data()));
				}
				if (command == "DIMENSION")
				{
					file >> command;
					gatePtr->ChangeDimenstion(command);
				}

				file >> command;
			}
			AddGate(gatePtr);
			gatePtr = NULL;
		}
		if (command == "CHARACTER")
		{
			characterPtr = new CCharacter();
			while (command != "END")
			{
				if (command == "NAME")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					characterPtr->ChangeName(command);
				}
				if (command == "SURNAME")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					characterPtr->ChangeSurname(command);
				}
				if (command == "PROFESSION")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					characterPtr->ChangeProfession(command);
				}
				if (command == "HP")
				{
					file >> command;
					characterPtr->SetMaxHP(atoi(command.data()));
				}
				if (command == "SANITY")
				{
					file >> command;
					characterPtr->SetMaxSanity(atoi(command.data()));
				}
				if (command == "STARTLOCATION")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					characterPtr->ChangeStartingPosition(command);
				}
				if (command == "SPEED")
				{
					file >> command;
					file >> tmpString;//EOL
					file >> tmpString;//SNEAK
					file >> tmpString;//NUMBER
					characterPtr->SetFirstStats(atoi(command.data()), atoi(tmpString.data()));
				}
				if (command == "FIGHT")
				{
					file >> command;
					file >> tmpString;//EOL
					file >> tmpString;//SNEAK
					file >> tmpString;//NUMBER
					characterPtr->SetSecondsStats(atoi(command.data()), atoi(tmpString.data()));
				}
				if (command == "LORE")
				{
					file >> command;
					file >> tmpString;//EOL
					file >> tmpString;//SNEAK
					file >> tmpString;//NUMBER
					characterPtr->SetThirdStats(atoi(command.data()), atoi(tmpString.data()));
				}
				if (command == "FOCUS")
				{
					file >> command;
					characterPtr->SetFocus(atoi(command.data()));
				}
				file >> command;
			}
			AddCharacter(characterPtr);
			characterPtr = NULL;
		}
		if (command == "MYTHCARD")
		{
			CardPtr = new CCardMyth();
			while (command != "END")
			{
				if (command == "GATE")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					CardPtr->ChangeGateLocation(command);
				}
				if (command == "CLUE")
				{
					file >> command;
					file >> tmpString;
					while (tmpString != "EOL")
					{
						command.append(" ");
						command.append(tmpString);
						file >> tmpString;
					}
					CardPtr->ChangeClueLocation(command);
				}
				if (command == "WHITEPATH")
				{
					file >> command;
					CardPtr->AddWhiteMovement(command);
				}
				if (command == "BLACKPATH")
				{
					file >> command;
					CardPtr->AddBlackMovement(command);
				}
				if (command == "SCRIPT")
				{
					EventPtrRoot = new CMyEvent();
					EventPtr = EventPtrRoot;
					while (command != "END") 
					{
						file >> command;
						if (command == "TEXT")
						{
							EventPtr->AddEventCommnand(command);
							file >> command;
							file >> tmpString;
							while (tmpString != "EOL")
							{
								command.append(" ");
								command.append(tmpString);
								file >> tmpString;
							}
							EventPtr->AddEventCommnand(command);
							EventPtr = EventPtr->CreateNextEvent();
						}
						if (command == "GTAO")
						{
							EventPtr->AddEventCommnand(command);
							file >> command;
							EventPtr->AddEventCommnand(command);
							file >> command;
							EventPtr->AddEventCommnand(command);
							file >> command;
							file >> tmpString;
							while (tmpString != "STOP")
							{
								command.append(" ");
								command.append(tmpString);
								file >> tmpString;
							}
							EventPtr->AddEventCommnand(command);
							file >> command;
							EventPtr->AddEventCommnand(command);
							file >> command;
							EventPtr->AddEventCommnand(command);
							EventPtr = EventPtr->CreateNextEvent();
						}
						if (command == "MTB")
						{
							EventPtr->AddEventCommnand(command);
							file >> command;
							EventPtr->AddEventCommnand(command);
							file >> command;
							EventPtr->AddEventCommnand(command);
							EventPtr = EventPtr->CreateNextEvent();
						}
						if (command == "STOP")
						{
							EventPtr->AddEventCommnand(command);
							EventPtr = EventPtr->CreateNextEvent();
						}
					}

					CardPtr->ChangeScript(*EventPtrRoot);
					EventPtr = NULL;
				}
				file >> command;
			}
			AddCard(CardPtr);
			CardPtr = NULL;
		}
	}

	file.close();
	Shuffle();
	return true;
}

bool CResource::FlushData()
{
	for (int i = 0; i < MAXRESOURCENUMBER; i++)
	{
		m_pMonsterArray[i] = NULL;
		m_pGateArray[i] = NULL;
		m_pMythArray[i] = NULL;
		m_RClues[i] = NULL;
		m_RElderGod[i] = NULL;
		m_RHealth[i] = NULL;
		m_RMoney[i] = NULL;
		m_RSanity[i] = NULL;
		m_RPlayers[i] = NULL;
		m_RSealSign[i] = NULL;
	}
	return true;
}

bool CResource::Shuffle()
{
	int counter = 0;
	int tmpint = 0;

	CMonster *ptr = NULL;

	srand((unsigned int)time(NULL));

	for (counter = 0; counter < MAXRESOURCENUMBER; counter++)
	{
		if (m_pMonsterArray[counter] == NULL)
		{
			counter--;
			break;
		}
	}
	for (int i = 0; i < counter; i++)
	{
		tmpint = rand() % counter;
		ptr = m_pMonsterArray[i];
		m_pMonsterArray[i] = m_pMonsterArray[tmpint];
		m_pMonsterArray[tmpint] = ptr;
	}

	return false;
}

CResource::CResource()
{
	FlushData();
}


CResource::~CResource()
{
}
