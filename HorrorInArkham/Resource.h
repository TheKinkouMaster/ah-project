#pragma once
#include <string>
#include <fstream>
#include <iostream>

#include "GameObject.h"

#include "ItemAbility.h"
#include "ItemAlly.h"
#include "ItemCommon.h"
#include "ItemRare.h"
#include "ItemSpell.h"
#include "Token.h"

#include "Ancient.h"
#include "Character.h"
#include "Gate.h"
#include "Monster.h"

#include "CardMyth.h"

using namespace std;

class CResource :
	public CGameObject
{
private:
	CMonster *m_pMonsterArray[MAXRESOURCENUMBER];
	CGate *m_pGateArray      [MAXRESOURCENUMBER];
	CCardMyth *m_pMythArray  [MAXRESOURCENUMBER];

	CToken *m_RMoney   [MAXRESOURCENUMBER];
	CToken *m_RClues   [MAXRESOURCENUMBER];
	CToken *m_RSanity  [MAXRESOURCENUMBER];
	CToken *m_RHealth  [MAXRESOURCENUMBER];
	CToken *m_RElderGod[MAXRESOURCENUMBER];
	CToken *m_RSealSign[MAXRESOURCENUMBER];

	CCharacter *m_RPlayers[MAXRESOURCENUMBER];


	bool SwapObjects(CGameObject *first, CGameObject *second);
public:
	//Accessors
	CMonster *GiveMonsterPtr(int which = 0);
	CMonster *GiveMonster();
	CMonster *GiveMonster(string location);

	CGate *GiveGate();
	CGate *GiveGate(CGate *ptrToGate);
	CGate *GiveGatePtr(int which);
	CGate *GiveGatePtr(string location);

	CToken *GiveToken(string command);
	CToken *GiveTokenPtr(string command, int which);

	CCardMyth *GiveMythCard();

	CCharacter *GiveCharacter(int which = -1);
	CCharacter *GiveCharacterPtr(int which = -1);
	CCharacter *GiveCharacterPtr(string surname);

	

	//Modifiers
	bool AddCard(CCardMyth *card);
	bool AddMonster(CMonster *monster);
	bool AddGate(CGate *gate);
	bool AddResource(int number, string name);
	bool AddResource(string name, CGameObject *ptrToResource);
	bool AddCharacter(CCharacter *character);
	bool DeleteMonster(CMonster *ptr);


	

	//Methods
	bool SwapCard();

	int returnPosition(const char *name);

	bool ImportData(const char *filename);

	bool FlushData();
	bool Shuffle();

	//C/S
	CResource();
	~CResource();
};

