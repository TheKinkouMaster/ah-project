#include "Monster.h"



CMonster::CMonster()
{
	m_iAwareness = 0;
	m_iHealth = 0;
	m_iPhysicalDMG = 0;
	m_iPhysicalMod = 0;
	m_iSanityDMG = 0;
	m_iSanityMod = 0;
	m_sMovement.isFast = false;
	m_sMovement.isFlying = false;
	m_sMovement.isNormal = false;
	m_sMovement.isSpecial = false;
	m_sMovement.isStatic = false;

	m_sAbilities.ambush = false;
	m_sAbilities.ethernal = false;
	m_sAbilities.magicInsensitivity = false;
	m_sAbilities.magicResistans = false;
	m_sAbilities.mask = false;
	m_sAbilities.nightmare = false;
	m_sAbilities.nightmareValue = 0;
	m_sAbilities.overwhelming = false;
	m_sAbilities.overwhelmingValue = 0;
	m_sAbilities.physicalInsensivitivity = false;
	m_sAbilities.physicalResistans = false;
	m_sAbilities.undead = false;
}


CMonster::~CMonster()
{
}

char * CMonster::GiveMovement()
{
	if (m_sMovement.isNormal == true)  return "Normal";
	if (m_sMovement.isFast == true)    return "Fast";
	if (m_sMovement.isFlying == true)  return "Flying";
	if (m_sMovement.isSpecial == true) return "Special";
	return "Undefined";
}

string CMonster::GiveDimention()
{
	if (m_sDimension.circle == true)  return "Circle";
	if (m_sDimension.cross == true)   return "Cross";
	if (m_sDimension.diamond == true) return "Diamond";
	if (m_sDimension.hexagon == true) return "Hexagon";
	if (m_sDimension.line == true)    return "Line";
	if (m_sDimension.square == true)  return "Square";
	if (m_sDimension.star == true)    return "Star";
	if (m_sDimension.triangle == true)return "Triangle";
	if (m_sDimension.moon == true)    return "Moon";
	return "Undefined";
}

SAbilities CMonster::GiveAbilities()
{
	return m_sAbilities;
}

bool CMonster::ChangeMovement(string movementName)
{ 
	m_sMovement.isFast    = false;
	m_sMovement.isFlying  = false;
	m_sMovement.isNormal  = false;
	m_sMovement.isStatic  = false;
	m_sMovement.isSpecial = false;
	if (movementName == "Normal")       m_sMovement.isNormal  = true;
	else if (movementName == "Flying")  m_sMovement.isFlying  = true;
	else if (movementName == "Fast")    m_sMovement.isFast    = true;
	else if (movementName == "Special") m_sMovement.isSpecial = true;
	else if (movementName == "Static")  m_sMovement.isStatic  = true;
	else return false;
	return true;
}

bool CMonster::ChangeDimension(string newDimension)
{
	m_sDimension.circle   = false;
	m_sDimension.cross    = false;
	m_sDimension.diamond  = false;
	m_sDimension.hexagon  = false;
	m_sDimension.line     = false;
	m_sDimension.square   = false;
	m_sDimension.star     = false;
	m_sDimension.triangle = false;
	m_sDimension.moon     = false;
	if (newDimension == "Circle")       m_sDimension.circle   = true;
	else if (newDimension == "Cross")   m_sDimension.cross    = true;
	else if (newDimension == "Diamond") m_sDimension.diamond  = true;
	else if (newDimension == "Hexagon") m_sDimension.hexagon  = true;
	else if (newDimension == "Line")    m_sDimension.line     = true;
	else if (newDimension == "Square")  m_sDimension.square   = true;
	else if (newDimension == "Star")    m_sDimension.star     = true;
	else if (newDimension == "Triangle")m_sDimension.triangle = true;
	else if (newDimension == "Moon")    m_sDimension.moon     = true;
	else return false;
	return true;
}

bool CMonster::ChangeAbilities(string newAbility, int val)
{   
	if      (newAbility == "Ambush")  m_sAbilities.ambush             = true;
	else if (newAbility == "Ethernal")m_sAbilities.ethernal           = true;
	else if (newAbility == "MagicIns")m_sAbilities.magicInsensitivity = true;
	else if (newAbility == "MagicRes")m_sAbilities.magicResistans     = true;
	else if (newAbility == "Undead")  m_sAbilities.undead             = true;
	else if (newAbility == "Mask")    m_sAbilities.mask               = true;
	else if (newAbility == "Nightmare")
	{
		m_sAbilities.nightmare      = true;
		m_sAbilities.nightmareValue = val;
	}
	else if (newAbility == "Overwhelming")
	{
		m_sAbilities.overwhelming      = true;
		m_sAbilities.overwhelmingValue = val;
	}
	else if (newAbility == "PhysicalIns")m_sAbilities.physicalInsensivitivity = true;
	else if (newAbility == "PhysicalRes")m_sAbilities.physicalResistans       = true;
	else return false;
	return true;
}

bool CMonster::ChangeDescription(string newDesc)
{
	m_sDescription = newDesc;
	return true;
}
