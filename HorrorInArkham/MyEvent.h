#pragma once
#include "MyMap.h"
#include "GameObject.h"
class CMyEvent
{
private:
	string    m_sCommand[8];
	CMyEvent *m_pNext;
	CMyEvent *m_pAdditional;
	CMyEvent *m_pPrev;
public:
	//Accessors
	string   *GiveCommandLine()   { return m_sCommand; };
	CMyEvent *GiveNextPtr()       { return m_pNext; };
	CMyEvent *GiveAdditionalPtr() { return m_pAdditional; };
	CMyEvent *GivePrevPtr()       { return m_pPrev; };

	bool      CheckNext();
	bool      CheckAdditional();
	bool      CheckPrev();
	//Modifiers
	bool      AddEventCommnand(string newKeyword);

	CMyEvent* CreateAdditionalEvent()      { m_pAdditional = new CMyEvent; m_pAdditional->LinkPrevItem(this); return m_pAdditional; };
	inline void LinkPrevItem(CMyEvent *ptr){ m_pPrev = ptr; };
	CMyEvent* CreateNextEvent()            { m_pNext = new CMyEvent; m_pNext->LinkPrevItem(this);  return m_pNext; };
	//Methods

	//C/D
	CMyEvent();
	~CMyEvent();
};

